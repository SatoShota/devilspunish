﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackGroundMusic : MonoBehaviour
{
    //各シーン名
    private const string TITLE_SCENE = "TitleScene";
    private const string PLAY_SCENE = "PlayScene";
    private const string RESULT_SCENE = "ResultScene";

    //タイトルのBGM 1が暗雲時 2が晴れたとき
    //public AudioClip titleBgm1_;
    public AudioClip titleBgm2_;

    //プレイシーンのBGM
    public AudioClip playBgm_;

    //リザルトシーンのBGM
    public AudioClip resultBgm_;

    //オーディオクラスオブジェクト(これを使ってSEを流す)
    AudioSource audioSource_;

    //一番最初の自分のオブジェクトを記憶するためのオブジェクト
    public static BackGroundMusic instance_;

    //スタートより先に自分が死なないオブジェクトとして登録
    private void Awake()
    {
        //Componentを取得
        audioSource_ = GetComponent<AudioSource>();

        if (instance_ == null)
        {
            instance_ = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //各シーンの初めに呼んでもらうことでBGMが流れる
    public void ActBgm()
    {

        //各シーンによって流す効果音を変更
        switch (SceneManager.GetActiveScene().name)
        {
            case TITLE_SCENE:

                //タイトルシーンで流す曲をセット
                PlayBgm(titleBgm2_);

                break;
            case PLAY_SCENE:

                //プレイシーンで流す曲をセット
                PlayBgm(playBgm_);

                break;
            case RESULT_SCENE:

                //リザルトシーンで流す曲をセット
                PlayBgm(resultBgm_);
                break;
        }
    }

    //BGMを止める
    public void StopBgm()
    {
        //前シーンで流れていたBGMを停止
        audioSource_.Stop();
    }

    private void PlayBgm(AudioClip clip)
    {
        audioSource_.volume = 1.0f;

        //前シーンで流れていたBGMを停止
        audioSource_.Stop();

        //流したい音源を指定
        audioSource_.clip = clip;

        //再生
        audioSource_.Play();

        //ループ再生するように設定
        audioSource_.loop = true;
    }

    //晴天背景時のBGM再生
    public void TitleBgm2()
    {
        //前シーンで流れていたBGMを停止
        audioSource_.Stop();

        //流したい音源を指定
        audioSource_.clip = titleBgm2_;

        //再生
        audioSource_.Play();

        //ループ再生するように設定
        audioSource_.loop = true;
    }
}
