﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveSample : MonoBehaviour
{
    private const string KEY_NAME = "SCORE";

    //悪魔を倒したときに得られるスコア
    private const int DEVIL_POINT = 100;

    //邪神を倒したときに得られるスコア
    private const int EVILGOD_POINT = 1000;

    private const float LAEVA_BONUS = 1.1f;

    //スコア倍率
    private float bonus_ = 1.0f;

    private GameObject scoreObj_ = null;
    private Text scoreText_;
    private float scoreNum_ = 0;

    //フレーム最大値
    private const int FLAME_MAX = 60;

    //フレームカウント用変数
    private int flameCount_ = 0;

    //開始判定用フラグ
    bool start_ = false;

    //バックグラウンドオブジェクト作成
    GameObject scoreBack_;

    //プレイヤーオブジェクト作成
    GameObject player_;

    // Start is called before the first frame update
    void Start()
    {

        //スコアをロード
        //scoreNum_ = PlayerPrefs.GetInt(KEY_NAME, 0);

        //テキストをヒエラルキーから探す
        scoreObj_ = GameObject.Find("Text");

        //テキストのコンポーネント取得
        scoreText_ = scoreObj_.GetComponent<Text>();

        scoreBack_ = new GameObject("TimeBack"); 

        ImageLoad(scoreBack_, "TimeBackGround", new Vector3(720, 360, -100));

        //最背面へ
        scoreBack_.GetComponent<RectTransform>().SetAsFirstSibling();

        //プレイヤーオブジェクト検索
        player_ = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {

        if (start_ == true)
        {
            //60フレームカウント
            if (flameCount_ < FLAME_MAX)
            {
                flameCount_++;
            }
            else
            {
                //とりあえず加算
                //scoreNum_ += 100;
                flameCount_ = 0;
            }

            //テキストの表示を入れ替える
            scoreText_.text = "" + scoreNum_;
        }
        
    }

    //削除したときの処理
    public void OnDestroy()
    {
        //スコアを保存
        PlayerPrefs.SetInt(KEY_NAME, (int)scoreNum_);
        PlayerPrefs.Save();
    }

    //開始を通知してもらう関数
    public void SetStartFlag()
    {
        start_ = true;
    }

    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void ImageLoad(GameObject workImage, string fileName, Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("Canvas").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    public void SetScore(int devils)
    {
        if (player_.GetComponent<Player>().GetWeapon() == (int)WEAPON_TYPE.LAEVATEINN)
        {
            //倒されたのが悪魔だったら
            if (devils == (int)DEVILS.DEVIL)
            {
                scoreNum_ += (DEVIL_POINT * LAEVA_BONUS) * bonus_;
            }

            //倒されたのが邪神だったら
            else if (devils == (int)DEVILS.EVILGOD)
            {
                scoreNum_ += (EVILGOD_POINT * LAEVA_BONUS) * bonus_;
            }
        }
        else
        {
            //倒されたのが悪魔だったら
            if (devils == (int)DEVILS.DEVIL)
            {
                scoreNum_ += DEVIL_POINT * bonus_;
            }

            //倒されたのが邪神だったら
            else if (devils == (int)DEVILS.EVILGOD)
            {
                scoreNum_ += EVILGOD_POINT * bonus_;
            }
        }
    }

    //スキルによるスコア加算用
    public void SetSkillScore(int devil, int evil)
    {
        //もし選択した武器がレーヴァテインだったらパッシブボーナス付与した状態で計算
        if (player_.GetComponent<Player>().GetWeapon() == (int)WEAPON_TYPE.LAEVATEINN)
        {
            scoreNum_ += devil * (DEVIL_POINT * LAEVA_BONUS) * bonus_;
            scoreNum_ += evil * (EVILGOD_POINT * LAEVA_BONUS) * bonus_;
        }
        else
        {
            scoreNum_ += devil * DEVIL_POINT * bonus_;
            scoreNum_ += evil * EVILGOD_POINT * bonus_;
        }
    }

    //ボーナス追加
    public void SetBonus(float bonus)
    {
        if (bonus > bonus_)
        {
            bonus_ = bonus;
        }
    }

    public int GetScore()
    {
        return (int)scoreNum_;
    }
}
