﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckApplicationQuit : MonoBehaviour
{
    //ゲームを終了するかどうか
    private bool isApplicationQuit_ = false;

    //自分が存在するかどうか
    private static bool isExitCheckApplicationQuit_ = false;

    public void Awake()
    {
        //自分が存在するかどうかを確認
        if(isExitCheckApplicationQuit_)
        {
            //すでに存在しているのなら自分を削除
            Destroy(this.gameObject);
        }
        else
        {
            //まだ存在していないなら死なないオブジェクトとして登録
            isExitCheckApplicationQuit_ = true;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //アプリ終了時に呼ばれる
    public void OnApplicationQuit()
    {
        //終了することを記録
        isApplicationQuit_ = true;

        //保存データ全削除
        PlayerPrefs.DeleteAll();
    }

    //ゲームを終了するかどうかを教える
    public bool GetCheckFlag()
    {
        return isApplicationQuit_;
    }

    //自分が存在するかどうかを教える
    public bool GetISExitSelf()
    {
        return isExitCheckApplicationQuit_;
    }
}
