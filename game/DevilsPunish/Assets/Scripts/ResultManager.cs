﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultManager : MonoBehaviour
{
    //状態の列挙定数
    enum STATE : int
    {
        SHOW_RESULT,    //結果表示
        SHOW_RANKING,   //ランキング表示
        SCENE_CAHNGE    //シーン遷移する
    }

    //ランキングに記録している数
    private const int LOG_MAX = 5;

    //ドラムロールの待ち時間
    private const int WAIT_TIME = 180;

    //テキストの生成を止める印
    private const int STOP_SIGN = -1;

    //ボードの位置（X）
    private const float BOARD_POS_X = 1920.0f;

    //ボードの位置（Y）
    private const float BOARD_ADD_POS_Y = 180.0f;

    //ランキングボードの位置（Y）
    private const float RANKING_POS_Y = 450.0f;

    //パネルの透明度
    private const float PANEL_ALPHA = 0.5f;

    //複製元のボード
    public GameObject boardOrigin_;

    //複製したボードを格納する場所
    private GameObject[] board_ = new GameObject[LOG_MAX];

    //ランキング
    public GameObject rankingBoard_ = null;

    //時間を数える用
    private int timeCount_ = 0;

    //ボードの添え字
    private int boardIndex_ = 0;

    //リザルト画面の状態を示す
    private STATE state_ = STATE.SHOW_RESULT;

    //ボード達のY位置（定数を足したりする）
    private float boardPosY_ = 260.0f;

    //パネル
    private GameObject panel_ = null;

    //プレイヤーを格納
    private GameObject player_ = null;

    //セーブマネージャーを格納
    private GameObject saveManager_ = null;

    //BGMオブジェクトを格納
    private GameObject backGroundMusic_ = null;

    //SEオブジェクトを格納
    private GameObject soundEffect_ = null;

    //今回の武器名
    private string currentWeaponName_ = "";

    //今回のスコア
    private int currentScore_ = 0;

    //今回の悪魔撃破数
    private int currentDevilCount_ = 0;

    //今回の邪神撃破数
    private int currentEvilCount_ = 0;

    //スタートより早く呼んでほしいため
    private void Awake()
    {
        //プレイヤーを探す
        player_ = GameObject.Find("Player");

        //プレイヤーの情報を取得
        SetWeapon();
        SetScore();
        SetDevilCount();
        SetEvilGodCount();
    }

    // Start is called before the first frame update
    void Start()
    {
        //フレームレートを60に固定
        Application.targetFrameRate = 60;

        //パネルを取得
        panel_ = GameObject.Find("Panel");

        //RGBAを変更（実際は透明度のみを変更している）
        panel_.gameObject.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, 0.0f);

        //ボードを生成
        board_[boardIndex_] = CreateBoard(board_[boardIndex_], boardOrigin_);

        //ボードに名前を渡す
        board_[boardIndex_].GetComponent<ResultBoard>().SetWeaponName(currentWeaponName_);

        //ボードにスコアを渡す
        board_[boardIndex_].GetComponent<ResultBoard>().SetScore(currentScore_);

        //ボードに悪魔撃破数を渡す
        board_[boardIndex_].GetComponent<ResultBoard>().SetDevilKillCount(currentDevilCount_);

        //ボードに邪神撃破数を渡す
        board_[boardIndex_].GetComponent<ResultBoard>().SetEvilGodCount(currentEvilCount_);

        //中身を生成
        board_[boardIndex_].GetComponent<ResultBoard>().CreateContents();

        //セーブマネージャーを探す
        saveManager_ = GameObject.Find("SaveManager");

        //BGMオブジェクトを探す
        backGroundMusic_ = GameObject.Find("BackGroundMusic");

        //リザルトシーンBGMを流す
        backGroundMusic_.GetComponent<BackGroundMusic>().ActBgm();

        //SEオブジェクトを探す
        soundEffect_ = GameObject.Find("SoundEffect");

        //ドラムロールを鳴らす
        soundEffect_.GetComponent<SoundEffect>().ActSe();
    }

    // Update is called once per frame
    void Update()
    {
        //ゲームの状態によって動きを変える
        switch (state_)
        {
            case STATE.SHOW_RESULT:
                ShowResult();
                break;

            case STATE.SHOW_RANKING:
                ShowRanking();
                break;

            case STATE.SCENE_CAHNGE:
                SceneManager.LoadScene("TitleScene");
                break;
        }
    }

    //結果表示の動き
    public void ShowResult()
    {
        //待ち時間を超えたら
        if (timeCount_ > WAIT_TIME)
        {
            //最初（現在）の記録のスコアなどを生成
            board_[0].GetComponent<ResultBoard>().AddTextObject();

            //数字（フォント）の色を指定：黒
            board_[0].GetComponent<ResultBoard>().SetNumberColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));

            timeCount_ = STOP_SIGN;
        }
        else if (timeCount_ <= STOP_SIGN)
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                //ランキング画面へ
                state_ = STATE.SHOW_RANKING;

                //リセット
                timeCount_ = 0;

                //画面外に退散させる
                board_[0].GetComponent<RectTransform>().localPosition = new Vector3(BOARD_POS_X, boardPosY_, 0.0f);
            }
        }
        else
        {
            //カウントアップ
            timeCount_++;
        }
    }

    //ランキング表示の動き
    public void ShowRanking()
    {
        if (boardIndex_ != LOG_MAX)
        {
            //パネルの透過度を半分にする
            panel_.gameObject.GetComponent<Image>().color = new Color(0.0f, 0.0f, 0.0f, PANEL_ALPHA);

            //「ランキング」（テキスト）を生成
            rankingBoard_ = CreateBoard(rankingBoard_, rankingBoard_);

            //ランキングの位置調整
            rankingBoard_.GetComponent<RectTransform>().localPosition = new Vector3(0.0f, RANKING_POS_Y, 0.0f);
            
            //数字（フォント）の色を指定：黒
            board_[boardIndex_].GetComponent<ResultBoard>().SetNumberColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));

            //一旦削除
            Destroy(board_[boardIndex_]);

            //後から以後の記録の設定をする
            for (boardIndex_ = 0; boardIndex_ < LOG_MAX; boardIndex_++)
            {
                //ボードを生成していく
                board_[boardIndex_] = CreateBoard(board_[boardIndex_], boardOrigin_);

                //セーブマネージャーから武器を取得
                board_[boardIndex_].GetComponent<ResultBoard>().SetWeaponName(
                    saveManager_.GetComponent<SaveManager>().GetWeaponName(boardIndex_));

                //セーブマネージャーからスコアを取得
                board_[boardIndex_].GetComponent<ResultBoard>().SetScore(
                    saveManager_.GetComponent<SaveManager>().GetScore(boardIndex_));

                //セーブマネージャーから悪魔撃破数を取得
                board_[boardIndex_].GetComponent<ResultBoard>().SetDevilKillCount(
                    saveManager_.GetComponent<SaveManager>().GetDevilCount(boardIndex_));

                //セーブマネージャーから邪神撃破数を取得
                board_[boardIndex_].GetComponent<ResultBoard>().SetEvilGodCount(
                    saveManager_.GetComponent<SaveManager>().GetEvilGodCount(boardIndex_));

                //セーブマネージャーから今回の結果かどうかを教えてもらう
                board_[boardIndex_].GetComponent<ResultBoard>().SetEvilGodCount(
                    saveManager_.GetComponent<SaveManager>().GetEvilGodCount(boardIndex_));

                //今見ているボードのスコアが今回のスコアと同じなら
                if (saveManager_.GetComponent<SaveManager>().GetIsCurrentResult(boardIndex_))
                {
                    //背景を作成する
                    board_[boardIndex_].GetComponent<ResultBoard>().CreateBackGround();
                }

                //ボードの中身を生成する
                board_[boardIndex_].GetComponent<ResultBoard>().CreateContents();
                board_[boardIndex_].GetComponent<ResultBoard>().AddTextObject();

                //位置調整
                board_[boardIndex_].GetComponent<RectTransform>().localPosition = new Vector3(BOARD_POS_X, boardPosY_, 0.0f);
                
                //順位を決める
                board_[boardIndex_].GetComponent<ResultBoard>().SetRank(boardIndex_ + 1);

                //動くフラグON
                board_[boardIndex_].GetComponent<ResultBoard>().MoveFlagOn();

                //サイズや位置を変更する（一瞬の動き）
                board_[boardIndex_].GetComponent<ResultBoard>().MoveContents();

                //数字（フォント）の色を指定：黒
                board_[boardIndex_].GetComponent<ResultBoard>().SetNumberColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));

                //Y位置を決める
                boardPosY_ -= BOARD_ADD_POS_Y;
            }
        }

        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            //「シーン遷移」に移行
            state_ = STATE.SCENE_CAHNGE;
        }
    }

    //ボードを生成する
    public GameObject CreateBoard(GameObject obj, GameObject originObj)
    {
        obj = (GameObject)Instantiate(originObj) as GameObject;

        //親を設定（キャンバス）
        obj.transform.SetParent(GameObject.Find("Canvas").transform);

        //アンカーポジションの設定
        obj.GetComponent<RectTransform>().anchoredPosition = new Vector3(0.0f, 0.0f, 0.0f);

        //縮尺を等倍にする
        obj.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

        //戻り値として作成したオブジェクトを指定
        return obj;
    }

    /////////////////////// セッター群 ///////////////////////
    //プレイヤーから武器の種類を取得し、
    //該当する武器名を格納する
    public void SetWeapon()
    {
        //プレイヤーから武器の種類を取得して、該当する武器名を格納
        switch (player_.GetComponent<Player>().GetWeapon())
        {
            case (int)WEAPON_TYPE.LAEVATEINN:
                currentWeaponName_ = "Laevateinn";
                break;

            case (int)WEAPON_TYPE.MJOLNIR:
                currentWeaponName_ = "Mjolnir";
                break;
        }
    }

    //プレイヤーからスコアを取得する
    public void SetScore()
    {
        currentScore_ = player_.GetComponent<Player>().GetScore();
    }

    //プレイヤーから悪魔を倒した数を取得する
    public void SetDevilCount()
    {
        currentDevilCount_ = player_.GetComponent<Player>().GetKillDevil();
    }

    //プレイヤーから邪神を倒した数を取得する
    public void SetEvilGodCount()
    {
        currentEvilCount_ = player_.GetComponent<Player>().GetKillEvilgod();
    }


    /////////////////////// ゲッター群 ///////////////////////
    //武器名を渡す
    public string GetCurrentWeaponName()
    {
        return currentWeaponName_;
    }

    //プレイヤーからスコアを渡す
    public int GetCurrentScore()
    {
        return currentScore_;
    }

    //プレイヤーから悪魔を倒した数を渡す
    public int GetCurrentDevilCount()
    {
        return currentDevilCount_;
    }

    //プレイヤーから邪神を倒した数を渡す
    public int GetCurrentEvilGodCount()
    {
        return currentEvilCount_;
    }
}
