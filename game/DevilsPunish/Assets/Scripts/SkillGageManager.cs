﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillGageManager : MonoBehaviour
{
    const float SKILL_GAGE_MAX = 100.0f;    //最大のスキルゲージ

    GameObject gage_;   //スキルゲージのゲージの情報を取得用
    float currentGage_; //現在のスキルゲージ
    float MaxGage_;    //最大のスキルゲージ
    GameObject SkillGageM_; //ミョルニル
    GameObject SkillGageL_; //レーヴァティン
    RectTransform skillGageColor_;      //スキルゲージ
    GameObject skillGageBack_;
    GameObject ScoreBackGround;
    GameObject soundEffect_;        //効果音オブジェクト

    // Start is called before the first frame update
    void Start()
    {
        soundEffect_ = GameObject.Find("SoundEffect");
        skillGageColor_ = GameObject.Find("skillGageColor").GetComponent<RectTransform>();
        skillGageBack_ = GameObject.Find("SkillBackGround");
        ScoreBackGround = GameObject.Find("ScoreBackGround");
        skillGageBack_.transform.Translate(new Vector3(5, 0, 0));
        ScoreBackGround.transform.Translate(new Vector3(5, 0, 0));

        //選択した武器がミョルニルなら
        if (WeaponCursor.weapon_ == (int)WEAPON_TYPE.MJOLNIR)
        {
            SkillGageM_ = new GameObject("SkillGage");
            ImageLoad(SkillGageM_, "SkillGage_1", new Vector3(-716, -160, 0));
            skillGageColor_.localPosition = new Vector3(-716, -160, 0);

        }
        else
        {
            SkillGageL_ = new GameObject("SkillGage");
            ImageLoad(SkillGageL_, "SkillGage_2", new Vector3(-716, -160, 0));
            skillGageColor_.localPosition = new Vector3(-716, -160, 0);
        }

        gage_ = GameObject.Find("skillGageColor");

        currentGage_ = 0;
        MaxGage_ = 100.0f;
    }

    // Update is called once per frame
    void Update()
    {

        if (currentGage_ > SKILL_GAGE_MAX)
        {
            currentGage_ = SKILL_GAGE_MAX;
        }

       gage_.GetComponent<Image>().fillAmount = currentGage_ / MaxGage_;
    }

    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void ImageLoad(GameObject workImage, string fileName, Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("Canvas").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    //スキルポイントを加算します
    public void AddSkillPoint(int a)
    {
        //スキルゲージが100%未満かつ、今回の加算でゲージが100%になる時
        if(currentGage_ < SKILL_GAGE_MAX && currentGage_ + a >= SKILL_GAGE_MAX)
        {
            //効果音再生
            soundEffect_.GetComponent<SoundEffect>().SkillMax();        
        }

        currentGage_ += a;
    }

    //現在のスキルポイントを０にします
    public void SkillPointReset()
    {
        currentGage_ = 0;
    }

    //必殺技が使えるかどうかを返します。
    public bool isAbleToSkill()
    {
        if(currentGage_ == SKILL_GAGE_MAX)
        {
            return true;
        }

        return false;
    }

}
