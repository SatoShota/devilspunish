﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//現在の移動状況を表す
enum MOVE_STATE
{
    MOVE_0, //タイトルシーン開始から武器が指定位置までの状態
    MOVE_1, //武器が到達後回転して背景が変わる状態
    MOVE_2, //背景が変わりタイトルロゴが登場する状態
    MOVE_3, //タイトルロゴが登場して武器とタイトルが上昇して指定位置までくる状態
    MOVE_4, //微調整
    MOVE_5, //すべてのアニメーションが終わって受付入力を受け付けている状態
    MOVE_6
}



public class TItleAnimeManager : MonoBehaviour
{
    const float CURSOR_MOVEMENT = 0.5f;    //カーソルが一秒あたりに移動する量
    const float WEAPON_ROTATE_ = 4.9f;     //一度に回転する量
    const float FRAME_MAX = 60;            //このゲームのフレーム数
    const float CURSOR_MOVEMENT_TIME = 30; //カーソルが移動する時間
    const float WEAPON_FINAL_POS = (FRAME_MAX * 1.5f * 4) + (FRAME_MAX * 3 * -4);
    const float WEAPON_FINAL_ROTATE = FRAME_MAX / 5 * WEAPON_ROTATE_;
    const float TITLE_FINAL_POS = -130 + (FRAME_MAX * 1.5f * 4);

    MOVE_STATE mOVE_STATE_ = MOVE_STATE.MOVE_0;
    RectTransform Laevateinn_;  //レーヴァティンのオブジェクト
    RectTransform Mjolnir_;     //ミョルニルのオブジェクト
    GameObject titleLogo_;      //ゲームタイトル
    GameObject weaponIcon_1;    //ミョルニル
    GameObject weaponIcon_1_;   //ミョルニル
    GameObject weaponIcon_2;    //レーバテイン
    GameObject cursor_R;        //Cursor右
    GameObject cursor_L;        //ursor左
    GameObject soundEffect_;    //効果音オブジェクト
    GameObject audioBgm_;       //BGMオブジェクト
    float weaponRotate_ = 0;    //武器の回転の角度
    int weaponType = 0;         //使用している武器
    RectTransform text_;        //テキスト  
    bool cursorflag_ = false;   //カーソルを動かす際に使う
    float cursorMoveCount_ = 0; //カーソルが移動してるフレーム数
    int animeFrameCount = 0;    //現在のアニメーションのframeカウント
    float laevaPosX_ = 0.0f;
    float mjolPosX_ = 0.0f;

    float laevaPosY_ = 0.0f;
    float mjolPosY_ = 0.0f;

    public string titleLogoName_ = "TitleLogo_10";

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;

        //各オブジェクトを取得
        Laevateinn_ = GameObject.Find("Laevateinn").GetComponent<RectTransform>();
        Mjolnir_ = GameObject.Find("Mjolnir").GetComponent<RectTransform>();
        titleLogo_ = new GameObject("TitleLogo");
        weaponIcon_1_ = new GameObject("hammer");
        cursor_R = new GameObject("cursor_R");
        cursor_L = new GameObject("cursor_L");
        text_ = GameObject.Find("Text").GetComponent<RectTransform>();
        soundEffect_ = GameObject.Find("SoundEffect");
        audioBgm_ = GameObject.Find("BackGroundMusic");

        laevaPosX_ = Laevateinn_.GetComponent<RectTransform>().localPosition.x;
        mjolPosX_ = Mjolnir_.GetComponent<RectTransform>().localPosition.x;

        laevaPosY_ = Laevateinn_.GetComponent<RectTransform>().localPosition.y;
        mjolPosY_ = Mjolnir_.GetComponent<RectTransform>().localPosition.y;
    }

    // Update is called once per frame
    void Update()
    {
        //もし、アニメーション中にエンターが押されたら、すべてのアニメーションを終了して、入力受付状態に移行する
        if (Input.GetKeyDown(KeyCode.KeypadEnter) && mOVE_STATE_ < MOVE_STATE.MOVE_5)
        {
            SkipAnim();
        }

        //現在の状況に合わせて各画像を移動
        switch (mOVE_STATE_)
        {
            case MOVE_STATE.MOVE_0:

                //指定位置まで移動
                Laevateinn_.localPosition += new Vector3(0, -4, 0);
                Mjolnir_.localPosition += new Vector3(0, -4, 0);

                break;
            case MOVE_STATE.MOVE_1:

                //5°ずつ回転させる
                Laevateinn_.localRotation = Quaternion.Euler(0, 0, -weaponRotate_);
                Mjolnir_.localRotation = Quaternion.Euler(0, 0, weaponRotate_);

                //指定度数ずつ増やして徐々に回転
                weaponRotate_ += WEAPON_ROTATE_;

                break;
            case MOVE_STATE.MOVE_2:

                //暗い背景削除
                if (GameObject.Find("TitleBackGround_1") != null)
                {
                    //武器交差時効果音を鳴らす
                    soundEffect_.GetComponent<SoundEffect>().CrossSe();

                    //BGM切り替え
                    //audioBgm_.GetComponent<BackGroundMusic>().TitleBgm2();

                    Destroy(GameObject.Find("TitleBackGround_1"));
                }

                //タイトルロゴを表示
                if (titleLogo_.transform.parent != GameObject.Find("Canvas").transform)
                {
                    ImageLoad(titleLogo_, titleLogoName_, new Vector3(0, -130, 0));
                }

                break;
            case MOVE_STATE.MOVE_3:

                //二つの武器とタイトル画像を上昇させる
                Laevateinn_.localPosition += new Vector3(0, 4, 0);
                Mjolnir_.localPosition += new Vector3(0, 4, 0);
                titleLogo_.GetComponent<RectTransform>().localPosition += new Vector3(0, 4, 0);

                break;
            case MOVE_STATE.MOVE_4:

                //カーソルとテキストを表示
                if (cursor_L.transform.parent != GameObject.Find("Canvas").transform)
                {
                    ImageLoad(cursor_R, "SelectWeaponCursor_R", new Vector3(250, -225, 0));
                    ImageLoad(cursor_L, "SelectWeaponCursor_L", new Vector3(-250, -225, 0));
                    text_.localPosition = new Vector3(0, -475, 0);
                }

                break;
            case MOVE_STATE.MOVE_5:

                //最初に指定されているレーヴァティンを表示
                if (weaponType == 0)
                {
                    weaponIcon_2 = new GameObject("weaponIcon_2");
                    ImageLoad(weaponIcon_2, "SelectWeaponIcon_2", new Vector3(0, -225, 0));
                    Destroy(weaponIcon_1);

                    weaponType = 1;
                }

                //選択武器の変更した際のアイコン変更処理
                if (Input.GetKeyDown(KeyCode.Keypad6) || Input.GetKeyDown(KeyCode.Keypad4))
                {
                    if (weaponType == 1)
                    {
                        //武器アイコンを表示
                        weaponIcon_1 = new GameObject("weaponIcon_1");
                        ImageLoad(weaponIcon_1, "SelectWeaponIcon_1", new Vector3(0, -225, 0));
                        Destroy(weaponIcon_2);

                        weaponType = 2;

                        //効果音を鳴らす
                        soundEffect_.GetComponent<SoundEffect>().ActSe();
                    }
                    else
                    {
                        weaponIcon_2 = new GameObject("weaponIcon_2");
                        ImageLoad(weaponIcon_2, "SelectWeaponIcon_2", new Vector3(0, -225, 0));
                        Destroy(weaponIcon_1);

                        weaponType = 1;

                        //効果音を鳴らす
                        soundEffect_.GetComponent<SoundEffect>().ActSe();
                    }
                }

                //カーソルの拡大縮小処理
                { 
                    //カーソルの拡大率をリセット
                    cursor_R.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
                    cursor_L.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

                    //それぞれのキーが押されたら拡大
                    if (Input.GetKeyDown(KeyCode.Keypad6))
                    {
                        cursor_R.GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1.2f);
                    }
                    if (Input.GetKeyDown(KeyCode.Keypad4))
                    {
                        cursor_L.GetComponent<RectTransform>().localScale = new Vector3(1.2f, 1.2f, 1.2f);
                    }

                    //カーソルの移動処理
                    if (cursorflag_ == false)
                    {
                        cursor_L.GetComponent<RectTransform>().localPosition += new Vector3(-CURSOR_MOVEMENT, 0, 0);
                        cursor_R.GetComponent<RectTransform>().localPosition += new Vector3(CURSOR_MOVEMENT, 0, 0);
                    }
                    else
                    {
                        cursor_L.GetComponent<RectTransform>().localPosition -= new Vector3(-CURSOR_MOVEMENT, 0, 0);
                        cursor_R.GetComponent<RectTransform>().localPosition -= new Vector3(CURSOR_MOVEMENT, 0, 0);
                    }

                    //秒数をカウント
                    cursorMoveCount_++;

                    //特定の秒数が経過したなら
                    if (cursorMoveCount_ >= CURSOR_MOVEMENT_TIME)
                    {
                        if (cursorflag_)
                        {
                            cursorflag_ = false;
                        }
                        else
                        {
                            cursorflag_ = true;
                        }

                        cursorMoveCount_ = 0;
                    }
                }
              

                break;
        }

        //状態変更処理
        {

            //アニメフレームが180フレームたったなら
            if (animeFrameCount >= (FRAME_MAX * 3) && mOVE_STATE_ == MOVE_STATE.MOVE_0)
            {
                //次の状態に移動
                mOVE_STATE_ = MOVE_STATE.MOVE_1;

                //アニメーションのカウントをリセット
                animeFrameCount = 0;
            }

            //アニメフレームが12フレームたったなら
            if ((animeFrameCount - 1) >= (FRAME_MAX / 5) && mOVE_STATE_ == MOVE_STATE.MOVE_1)
            {
                //次の状態に移動
                mOVE_STATE_ = MOVE_STATE.MOVE_2;

                //アニメーションのカウントをリセット
                animeFrameCount = 0;
            }

            //アニメフレームが120フレームたったなら
            if ((animeFrameCount - 1) >= (FRAME_MAX * 1) && mOVE_STATE_ == MOVE_STATE.MOVE_2)
            {
                //次の状態に移動
                mOVE_STATE_ = MOVE_STATE.MOVE_3;

                //アニメーションのカウントをリセット
                animeFrameCount = 0;
            }

            //アニメフレームが180フレームたったなら
            if ((animeFrameCount - 1) >= (FRAME_MAX * 1.5f) && mOVE_STATE_ == MOVE_STATE.MOVE_3)
            {
                //次の状態に移動
                mOVE_STATE_ = MOVE_STATE.MOVE_4;

                //アニメーションのカウントをリセット
                animeFrameCount = 0;
            }

            //アニメフレームが0フレームたったなら
            if ((animeFrameCount - 1) == (FRAME_MAX / FRAME_MAX) && mOVE_STATE_ == MOVE_STATE.MOVE_4)
            {
                //次の状態に移動
                mOVE_STATE_ = MOVE_STATE.MOVE_5;

                //アニメーションのカウントをリセット
                animeFrameCount = 0;
            }

            ////アニメフレームが0フレームたったなら
            //if ((animeFrameCount - 1) == (FRAME_MAX / FRAME_MAX) && mOVE_STATE_ == MOVE_STATE.MOVE_5)
            //{
            //    //次の状態に移動
            //    mOVE_STATE_ = MOVE_STATE.MOVE_5;
            //}

            //現在のフレーム数をカウント
            animeFrameCount += 1;
        }
    }

    private void CreateTitleLogo()
    {
        if (GameObject.Find("TitleBackGround_1") != null)
        {
            Destroy(GameObject.Find("TitleBackGround_1"));
        }

        //タイトルロゴを表示
        if(titleLogo_.transform.parent != GameObject.Find("Canvas").transform)
        {
            ImageLoad(titleLogo_, titleLogoName_, new Vector3(0, -130, 0));
        }
        
    }

    //アニメーションスキップ
    private void SkipAnim()
    {
        //武器交差前状態のときのみ、武器交差効果音を鳴らす
        if(MOVE_STATE.MOVE_2 > mOVE_STATE_)
        {
            //武器交差時効果音を鳴らす
            soundEffect_.GetComponent<SoundEffect>().CrossSe();
        }

        //武器の回転数を0に戻してから指定回転数に戻す
        Laevateinn_.localRotation = Quaternion.Euler(0, 0, 0);
        Mjolnir_.localRotation = Quaternion.Euler(0, 0, 0);

        //武器を回転終了状態にする
        Laevateinn_.localRotation = Quaternion.Euler(0, 0, -WEAPON_FINAL_ROTATE);
        Mjolnir_.localRotation = Quaternion.Euler(0, 0, WEAPON_FINAL_ROTATE);

        //武器を最終移動地点に移動させる
        Laevateinn_.localPosition = new Vector3(laevaPosX_, laevaPosY_ + WEAPON_FINAL_POS, 0);
        Mjolnir_.localPosition = new Vector3(mjolPosX_, mjolPosY_ + WEAPON_FINAL_POS, 0);

        //暗い背景が消えていないなら消してから表示
        //暗い背景削除
        if (GameObject.Find("TitleBackGround_1") != null)
        {
            Destroy(GameObject.Find("TitleBackGround_1"));
        }

        //タイトルロゴを表示
        if (titleLogo_.transform.parent != GameObject.Find("Canvas").transform)
        {
            ImageLoad(titleLogo_, titleLogoName_, new Vector3(0, TITLE_FINAL_POS, 0));
        }
        else
        {
            //既にタイトル画像が生成されていたら位置のみ修正
            titleLogo_.GetComponent<RectTransform>().localPosition = new Vector3(0, TITLE_FINAL_POS, 0);
        }

        //カーソルとテキストを表示
        if (cursor_L.transform.parent != GameObject.Find("Canvas").transform)
        {
            ImageLoad(cursor_R, "SelectWeaponCursor_R", new Vector3(250, -225, 0));
            ImageLoad(cursor_L, "SelectWeaponCursor_L", new Vector3(-250, -225, 0));
            text_.localPosition = new Vector3(0, -475, 0);
        }

        //状態を最終状態に移動
        mOVE_STATE_ = MOVE_STATE.MOVE_5;
    }


    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void ImageLoad(GameObject workImage, string fileName, Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("Canvas").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    public int GetMoveState()
    {
        return (int)mOVE_STATE_;
    }
}
