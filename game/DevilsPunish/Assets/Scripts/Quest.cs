﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quest : MonoBehaviour
{
    //最大フレーム
    private const int FLAME_MAX = 60;

    //フレームカウント
    private int flameCount_ = 0;

    //開始フラグ
    bool start_ = false;

    //秒数カウント
    private int secCnt_ = 0;

    //クエストそれぞれの表示座標
    private Vector3 QUEST_1 = new Vector3(720, 70, 0);
    private Vector3 QUEST_2 = new Vector3(720, -30, 0);
    private Vector3 QUEST_3 = new Vector3(720, -130, 0);
    private Vector3 QUEST_4 = new Vector3(720, -230, 0);
    private Vector3 QUEST_5 = new Vector3(720, -330, 0);
    private Vector3 QUEST_6 = new Vector3(720, -430, 0);

    //クエスト背景座標位置
    private Vector3 QUEST_BACK = new Vector3(720, -160, 0);

    //各レベル
    private const int LEVEL1 = 1;
    private const int LEVEL2 = 2;
    private const int LEVEL3 = 3;

    //クエストオブジェクト作成
    GameObject quest_;

    //クエストバックグラウンド作成
    GameObject questBack_;

    //プレイヤー情報受け取り用
    GameObject player_;

    //スコア情報受け取り用
    GameObject score_;

    //悪魔生成クラス情報受け取り用
    GameObject devils_;

    //ステージオブジェクト
    GameObject stage_;

    //効果音オブジェクト
    GameObject soundEffect_;

    //レベル別クエストリスト
    List<string> questLevel1_ = new List<string> { "Quest_1", "Quest_4", "Quest_7" };
    List<string> questLevel2_ = new List<string> { "Quest_2", "Quest_5", "Quest_8" };
    List<string> questLevel3_ = new List<string> { "Quest_3", "Quest_6", "Quest_9" };

    //クエスト内容
    List<int> questdataLevel1_ = new List<int> { 5,  1, 200};
    List<int> questdataLevel2_ = new List<int> { 10, 3, 400};
    List<int> questdataLevel3_ = new List<int> { 15, 6, 600};

    //報酬画像名テキスト
    List<string> rewardLevel1_ = new List<string> { "Reward_1", "Reward_4", "Reward_7" };
    List<string> rewardLevel2_ = new List<string> { "Reward_2", "Reward_5", "Reward_8" };
    List<string> rewardLevel3_ = new List<string> { "Reward_3", "Reward_6", "Reward_9" };

    //報酬リスト
    List<float> rewardDataLevel1_ = new List<float> { 1.2f, 4.0f,  1.0f };
    List<float> rewardDataLevel2_ = new List<float> { 1.5f, 6.0f,  2.0f };
    List<float> rewardDataLevel3_ = new List<float> { 2.0f, 10.0f, 4.0f };

    //選ばれたクエストの番号
    List<int> questIndexList_ = new List<int> { 0 };

    //選択されたクエスト名格納用変数
    string selectqQue_;

    //選択されたクエストのインデックス記憶用
    int questIndex_ = 0;

    //報酬オブジェクト格納用
    List<GameObject> rewardObj_ = new List<GameObject> { null };

    //報酬オブジェクトが生成されているかどうかのチェック
    List<bool> checkReward_ = new List<bool> { false };

    // Start is called before the first frame update
    void Start()
    {
        //リスト初期化
        questIndexList_.Clear();

        //報酬リスト
        rewardObj_.Clear();

        //報酬オブジェクト確認リスト初期化
        checkReward_.Clear();

        //プレイヤーオブジェクト検索
        player_ = GameObject.Find("Player");

        //スコアオブジェクト検索
        score_ = GameObject.Find("ScoreManager");

        //クエスト背景描画用オブジェクト作成
        questBack_ = new GameObject("QuestBack");

        //悪魔生成オブジェクト検索
        devils_ = GameObject.Find("DevilManager");

        //ステージオブジェクト検索
        stage_ = GameObject.Find("StageManager");

        //効果音オブジェクト検索
        soundEffect_ = GameObject.Find("SoundEffect");

        //画像読み込み
        ImageLoad(questBack_, "QuestBackGround", QUEST_BACK);

        //一つ目のクエストオブジェクト生成
        quest_ = new GameObject("Quest1");

        //クエストリストからランダムに選択
        questIndex_ = Random.Range(0, questLevel1_.Count);

        //リストから文字列取得
        selectqQue_ = questLevel1_[questIndex_];

        //クエストインデックスリストに格納
        questIndexList_.Add(questIndex_);

        //一つ目のクエストを表示
        ImageLoad(quest_, selectqQue_, QUEST_1);

        //オブジェクト作成
        CreateReward();

        quest_.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //クエスト同じやつ出さないために要素消してるけどそのせいでカウント合わなくなってるよ

        CheckQuest();

        //カウントダウン処理が終わっていたら
        if (start_ == true)
        {
            quest_.SetActive(true);

            //60フレーム毎に
            if (FLAME_MAX > flameCount_)
            {
                flameCount_++;
                CheckQuest();
            }
            else
            {
                CheckQuest();
                secCnt_++;
                flameCount_ = 0;

                if ((secCnt_ % 5) == 0 && secCnt_ != 0)
                {
                    CheckQuest();
                    switch (secCnt_ / 5)
                    {
                        case 1:
                            GameObject quest_2;
                            quest_2 = new GameObject("Quest2");

                            //クエストリストからランダムに選択
                            while (true)
                            {
                                int buf = questIndex_;
                                questIndex_ = Random.Range(0, questLevel1_.Count);
                                if(buf != questIndex_)
                                {
                                    break;
                                }
                            }

                            //リストから文字列取得
                            selectqQue_ = questLevel1_[questIndex_];

                            //クエストインデックスリストに格納
                            questIndexList_.Add(questIndex_);

                            ImageLoad(quest_2, selectqQue_, QUEST_2);

                            break;
                        case 2:
                            GameObject quest_3;
                            quest_3 = new GameObject("Quest3");

                            //クエストリストからランダムに選択
                            while (true)
                            {
                                int buf = questIndex_;
                                questIndex_ = Random.Range(0, questLevel2_.Count);
                                if (buf != questIndex_)
                                {
                                    break;
                                }
                            }

                            //リストから文字列取得
                            selectqQue_ = questLevel2_[questIndex_];

                            //クエストインデックスリストに格納
                            questIndexList_.Add(questIndex_);

                            ImageLoad(quest_3, selectqQue_, QUEST_3);

                            break;
                        case 3:
                            GameObject quest_4;
                            quest_4 = new GameObject("Quest4");

                            //クエストリストからランダムに選択
                            while (true)
                            {
                                int buf = questIndex_;
                                questIndex_ = Random.Range(0, questLevel2_.Count);
                                if (buf != questIndex_)
                                {
                                    break;
                                }
                            }

                            //リストから文字列取得
                            selectqQue_ = questLevel2_[questIndex_];

                            //クエストインデックスリストに格納
                            questIndexList_.Add(questIndex_);

                            ImageLoad(quest_4, selectqQue_, QUEST_4);

                            break;
                        case 4:
                            GameObject quest_5;
                            quest_5 = new GameObject("Quest5");

                            //クエストリストからランダムに選択
                            while (true)
                            {
                                int buf = questIndex_;
                                questIndex_ = Random.Range(0, questLevel3_.Count);
                                if (buf != questIndex_)
                                {
                                    break;
                                }
                            }

                            //リストから文字列取得
                            selectqQue_ = questLevel3_[questIndex_];

                            //クエストインデックスリストに格納
                            questIndexList_.Add(questIndex_);

                            ImageLoad(quest_5, selectqQue_, QUEST_5);

                            break;
                        case 5:
                            GameObject quest_6;
                            quest_6 = new GameObject("Quest6");

                            //クエストリストからランダムに選択
                            while (true)
                            {
                                int buf = questIndex_;
                                questIndex_ = Random.Range(0, questLevel3_.Count);
                                if (buf != questIndex_)
                                {
                                    break;
                                }
                            }

                            //リストから文字列取得
                            selectqQue_ = questLevel3_[questIndex_];

                            //クエストインデックスリストに格納
                            questIndexList_.Add(questIndex_);

                            ImageLoad(quest_6, selectqQue_, QUEST_6);

                            break;
                    }
                    
                }
                CheckQuest();
            }

            
        }
    }

    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void ImageLoad(GameObject workImage, string fileName, Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("Canvas").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    //どのリストを見ればよいかの判定
    private void CheckQuest()
    {
        for(int i = 0; i < questIndexList_.Count; i++)
        {

            switch(i)
            {
                case 0:
                    CheckListType(LEVEL1, questdataLevel1_[questIndexList_[i]], i);
                    break;
                case 1:
                    CheckListType(LEVEL1, questdataLevel1_[questIndexList_[i]], i);
                    break;
                case 2:
                    CheckListType(LEVEL2, questdataLevel2_[questIndexList_[i]], i);
                    break;
                case 3:
                    CheckListType(LEVEL2, questdataLevel2_[questIndexList_[i]], i);
                    break;
                case 4:
                    CheckListType(LEVEL3, questdataLevel3_[questIndexList_[i]], i);
                    break;
                case 5:
                    CheckListType(LEVEL3, questdataLevel3_[questIndexList_[i]], i);
                    break;
            }

        }
    }

    //クエストの種類別に処理
    private void CheckListType(int level, int quest, int index)
    {
        
        switch(questIndexList_[index])
        {
            //スコアボーナス
            case 0:
                if(stage_.GetComponent<Stage>().GetKillDevil() >= quest)
                {
                    //表示画像切り替え
                    ChangeReward(questIndexList_[index], index);

                    //報酬を通知
                    SendRewardScore(level, questIndexList_[index]);

                }
                break;

            //出現頻度ボーナス
            case 1:
                if(stage_.GetComponent<Stage>().GetKillEvilgod() >= quest)
                {
                    //表示画像切り替え
                    ChangeReward(questIndexList_[index], index);

                    //報酬を通知
                    SendRewardDevil(level, questIndexList_[index]);

                }
                break;

            //邪神出現率ボーナス
            case 2:
                if (score_.GetComponent<SaveSample>().GetScore() >= quest)
                {
                    //表示画像切り替え
                    ChangeReward(questIndexList_[index], index);

                    //報酬を表示
                    SendRewardEvilgod(level, questIndexList_[index]);                    
                }
                break;
        }
    }

    //報酬内容を通知する
    private void SendRewardScore(int level, int index)
    {
        //各レベルごとの報酬を通知
        switch(level)
        {
            case 1:
                score_.GetComponent<SaveSample>().SetBonus(rewardDataLevel1_[index]);
                break;
            case 2:
                score_.GetComponent<SaveSample>().SetBonus(rewardDataLevel2_[index]);
                break;
            case 3:
                score_.GetComponent<SaveSample>().SetBonus(rewardDataLevel3_[index]);
                break;
        }
    }

    //報酬内容を通知する
    private void SendRewardDevil(int level, int index)
    {
        switch (level)
        {
            case 1:
                devils_.GetComponent<DevilsGenerator>().SetBonusDevil((int)rewardDataLevel1_[index], 1);
                break;
            case 2:
                devils_.GetComponent<DevilsGenerator>().SetBonusDevil((int)rewardDataLevel2_[index], 2);
                break;
            case 3:
                devils_.GetComponent<DevilsGenerator>().SetBonusDevil((int)rewardDataLevel3_[index], 3);
                break;
        }
    }

    //報酬内容を通知する
    private void SendRewardEvilgod(int level, int index)
    {
        switch (level)
        {
            case 1:
                devils_.GetComponent<DevilsGenerator>().SetBonusEvilgod((int)rewardDataLevel1_[index], 1);
                break;
            case 2:
                devils_.GetComponent<DevilsGenerator>().SetBonusEvilgod((int)rewardDataLevel2_[index], 2);
                break;
            case 3:
                devils_.GetComponent<DevilsGenerator>().SetBonusEvilgod((int)rewardDataLevel3_[index], 3);
                break;
        }
    }

    private void ChangeReward(int num, int index)
    {
        switch(index)
        {
            case 0:
                if (checkReward_[index] == false)
                {
                    GameObject Reward;
                    Reward = new GameObject("Reward" + index + 1);
                    ImageLoad(Reward, rewardLevel1_[num], QUEST_1);
                    checkReward_[index] = true;

                    //達成効果音再生
                    soundEffect_.GetComponent<SoundEffect>().QuestRewardSe();
                }
                break;
            case 1:
                if (checkReward_[index] == false)
                {
                    GameObject Reward;
                    Reward = new GameObject("Reward" + index + 1);
                    ImageLoad(Reward, rewardLevel1_[num], QUEST_2);
                    checkReward_[index] = true;

                    //達成効果音再生
                    soundEffect_.GetComponent<SoundEffect>().QuestRewardSe();
                }
                break;
            case 2:
                if (checkReward_[index] == false)
                {
                    GameObject Reward;
                    Reward = new GameObject("Reward" + index + 1);
                    ImageLoad(Reward, rewardLevel2_[num], QUEST_3);
                    checkReward_[index] = true;

                    //達成効果音再生
                    soundEffect_.GetComponent<SoundEffect>().QuestRewardSe();
                }
                break;
            case 3:
                if (checkReward_[index] == false)
                {
                    GameObject Reward;
                    Reward = new GameObject("Reward" + index + 1);
                    ImageLoad(Reward, rewardLevel2_[num], QUEST_4);
                    checkReward_[index] = true;

                    //達成効果音再生
                    soundEffect_.GetComponent<SoundEffect>().QuestRewardSe();
                }
                break;
            case 4:
                if (checkReward_[index] == false)
                {
                    GameObject Reward;
                    Reward = new GameObject("Reward" + index + 1);
                    ImageLoad(Reward, rewardLevel3_[num], QUEST_5);
                    checkReward_[index] = true;

                    //達成効果音再生
                    soundEffect_.GetComponent<SoundEffect>().QuestRewardSe();
                }
                break;
            case 5:
                if (checkReward_[index] == false)
                {
                    GameObject Reward;
                    Reward = new GameObject("Reward" + index + 1);
                    ImageLoad(Reward, rewardLevel3_[num], QUEST_6);
                    checkReward_[index] = true;

                    //達成効果音再生
                    soundEffect_.GetComponent<SoundEffect>().QuestRewardSe();
                }
                break;
        }

    }

    public void SetStartFlag()
    {
        start_ = true;
    }

    private void CreateReward()
    {
        for(int i = 0; i < 6; i++)
        {
            checkReward_.Add(false);
        }
    }
}
