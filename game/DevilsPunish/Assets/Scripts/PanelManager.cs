﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PanelManager : MonoBehaviour
{
    GameObject game;
    float red;
    float blue;
    float green;

    // Start is called before the first frame update
    void Start()
    {
        red = GetComponent<Image>().color.r;
        green = GetComponent<Image>().color.g;
        blue = GetComponent<Image>().color.b;
    }
    // Update is called once per frame
    void Update()
    {
      

    }

    public void Panel_ON()
    {
        GetComponent<Image>().color = new Color(red, green, blue, 0.5f);
    }

    public void Panel_OFF()
    {
        GetComponent<Image>().color = new Color(red, green, blue, 0.0f);
    }

}
