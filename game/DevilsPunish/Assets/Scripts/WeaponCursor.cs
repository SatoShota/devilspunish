﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum WEAPON_TYPE
{
    MJOLNIR,
    LAEVATEINN,
}


public class WeaponCursor : MonoBehaviour
{
    WEAPON_TYPE weapoType_ = WEAPON_TYPE.LAEVATEINN;
    GameObject titleAnime_;
    public static int weapon_ = (int)WEAPON_TYPE.LAEVATEINN;

    // Start is called before the first frame update
    void Start()
    {
        titleAnime_ = GameObject.Find("TitleAnimeManager");

        weapon_ = (int)WEAPON_TYPE.LAEVATEINN;
    }

    // Update is called once per frame
    void Update()
    {
        if (titleAnime_.GetComponent<TItleAnimeManager>().GetMoveState() == (int)MOVE_STATE.MOVE_5)
        {
           if (Input.GetKeyDown(KeyCode.Keypad6) || Input.GetKeyDown(KeyCode.Keypad4))
            {
                if (weapoType_ == WEAPON_TYPE.MJOLNIR)
                {
                    weapoType_ = WEAPON_TYPE.LAEVATEINN;
                    weapon_ = (int)WEAPON_TYPE.LAEVATEINN;
                }
                else
                {
                    weapoType_ = WEAPON_TYPE.MJOLNIR;
                    weapon_ = (int)WEAPON_TYPE.MJOLNIR;
                }
            }
        }
    }
}
