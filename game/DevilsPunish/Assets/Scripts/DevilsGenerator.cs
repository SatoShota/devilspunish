﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
    
public class DevilsGenerator : MonoBehaviour
{
    //デビル表示座標
    private Vector3 SPAWN1_ = new Vector3(-240, 150, 0);
    private Vector3 SPAWN2_ = new Vector3(0, 150, 0);
    private Vector3 SPAWN3_ = new Vector3(240, 150, 0);
    private Vector3 SPAWN4_ = new Vector3(-240, -150, 0);
    private Vector3 SPAWN5_ = new Vector3(0, -150, 0);
    private Vector3 SPAWN6_ = new Vector3(240, -150, 0);
    private Vector3 SPAWN7_ = new Vector3(-240, -450, 0);
    private Vector3 SPAWN8_ = new Vector3(0, -450, 0);
    private Vector3 SPAWN9_ = new Vector3(240, -450, 0);

    List<string> fileName = new List<string> { "",
        "EnemyCanvas1", "EnemyCanvas1" , "EnemyCanvas1",
        "EnemyCanvas2", "EnemyCanvas2" , "EnemyCanvas2",
        "EnemyCanvas3", "EnemyCanvas3" , "EnemyCanvas3"
    };

    //ミョルニルパッシブボーナス
    private const int MJOL_BONUS = 1;

    //マスの最大
    const int MAP_MAX = 10;

    //フレームの最大
    const int FLAME_MAX = 60;

    //スポーン確立
    const int SPAWN_RATE = 30;

    //フレームカウント用
    int flameCnt_ = 30;

    //悪魔と邪神の出る割合
    int selectDev_ = 8;

    //各種報酬レベル受け取り
    int devilBonusLevel_ = 0;
    int evilBonusLevel_ = 0;

    //開始判定用フラグ
    bool start_ = false;

    //ランダム数値生成用
    System.Random respawn_ = new System.Random();   //位置
    System.Random select_ = new System.Random();    //種類

    //空いている位置を格納するリスト
    List<int> spawnList_ = new List<int> { 1, 2, 3};

    //悪魔たち生成速度
    int spawnTime = SPAWN_RATE;

    //ステージオブジェクト受け取り
    GameObject stage_;

    //プレイヤーオブジェクト作成
    GameObject player_;

    //スポーン位置格納用
    int spawn_ = 0;

    //生成位置格納用変数
    Vector3 resPos_;

    //悪魔のprefab
    public GameObject devilPrefab_;

    //邪神のprefab
    public GameObject evilgodPrefab_;

    // Start is called before the first frame update
    void Start()
    {
        stage_ = GameObject.Find("StageManager");

        spawnList_.Clear();

        //プレイヤーオブジェクト検索
        player_ = GameObject.Find("Player");

        if(player_.GetComponent<Player>().GetWeapon() == (int)WEAPON_TYPE.MJOLNIR)
        {
           selectDev_ -= MJOL_BONUS;
        }
    }

    // Update is called once per frame
    void Update()
    {

        //生成可能位置検索
        SetResPos();

        if (start_ == true)
        {
            //0.5秒毎に
            if (flameCnt_ >= spawnTime)
            {
                //リストからランダムに生成位置取得
                if (spawnList_.Count > 0)
                {
                    spawn_ = spawnList_[Random.Range(0, spawnList_.Count)];

                    //悪魔生成
                    CreateDevil(spawn_);

                    flameCnt_ = 0;
                }
            }

            //フレーム数カウント
            flameCnt_++;
        }
        
    }

    //悪魔を生成
    //引数     ：   呼び出す位置
    //戻り値   ：   なし
    private void CreateDevil(int num)
    {
        

        switch (num)
        {
            case 1:
                SetPos(SPAWN1_, num);
                break;
            case 2:
                SetPos(SPAWN2_, num);
                break;
            case 3:
                SetPos(SPAWN3_, num);
                break;
            case 4:
                SetPos(SPAWN4_, num);
                break;
            case 5:
                SetPos(SPAWN5_, num);
                break;
            case 6:
                SetPos(SPAWN6_, num);
                break;
            case 7:
                SetPos(SPAWN7_, num);
                break;
            case 8:
                SetPos(SPAWN8_, num);
                break;
            case 9:
                SetPos(SPAWN9_, num);
                break;

        }
    }

    private void SetPos(Vector3 pos, int index)
    {
        //出現確立を見て悪魔か邪神かを判定
        if (select_.Next(1, 10) <= selectDev_)
        {
            //悪魔を生成したことを通知
            stage_.GetComponent<Stage>().SetDevils(index, (int)DEVILS.DEVIL);

            //悪魔prefabを作成
            GameObject devil_ = Instantiate(devilPrefab_) as GameObject;

            //悪魔の名前に番号を付与
            devil_.gameObject.name = "Devil" + index;

            //自分の番号を教える
            devil_.GetComponent<Devil>().SetIndex(index);

            //作ったゲームオブジェクトをCanvasの子に
            devil_.transform.SetParent(GameObject.Find(fileName[index]).transform);

            //画像の位置(アンカーポジション)
            devil_.GetComponent<RectTransform>().anchoredPosition = pos;

            //縮尺がおかしいのでちゃんと等倍にする
            devil_.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            //アスペクト比は元の画像の比率を維持。
            devil_.GetComponent<Image>().preserveAspect = true;

            //画像のサイズを元画像と同じサイズにする。
            devil_.GetComponent<Image>().SetNativeSize();
        }
        else
        {
            //邪神を生成したことを通知
            stage_.GetComponent<Stage>().SetDevils(index, (int)DEVILS.EVILGOD);

            //悪魔prefabを作成
            GameObject devil_ = Instantiate(evilgodPrefab_) as GameObject;

            //邪神の名前に番号を付与
            devil_.gameObject.name = "Evil" + index;

            //自分の番号を教える
            devil_.GetComponent<EvilGod>().SetIndex(index);

            //作ったゲームオブジェクトをCanvasの子に
            devil_.transform.SetParent(GameObject.Find(fileName[index]).transform);

            //画像の位置(アンカーポジション)
            devil_.GetComponent<RectTransform>().anchoredPosition = pos;

            //縮尺がおかしいのでちゃんと等倍にする
            devil_.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            //アスペクト比は元の画像の比率を維持。
            devil_.GetComponent<Image>().preserveAspect = true;

            //画像のサイズを元画像と同じサイズにする。
            devil_.GetComponent<Image>().SetNativeSize();
        }
    }

    //リスポーン可能位置を確認する関数
    private void SetResPos()
    {
        //要素初期化
        spawnList_.Clear();

        //全要素を見るためにループ
        for(int i = 1; i < MAP_MAX; i++)
        {
            //ステージにどこが開いているかを聞く
            if(stage_.GetComponent<Stage>().CheckSpawn(i))
            {
                //空いている場所が見つかったら要素を追加する
                spawnList_.Add(i);
            }
        }
    }

    public void SetBonusDevil(int bonus, int level)
    {
        if (devilBonusLevel_ < level)
        {
            devilBonusLevel_ = level;
            spawnTime = spawnTime - bonus;
        }
    }

    public void SetBonusEvilgod(int bonus, int level)
    {
        if (evilBonusLevel_ < level)
        {
            evilBonusLevel_ = level;
            selectDev_ = selectDev_ - bonus;
        }
    }

    private void SelectCanvas()
    {

    }

    //ゲーム開始を通知してもらう関数
    public void SetStartFlag()
    {
        start_ = true;
    }

}
