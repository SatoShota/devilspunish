﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillEffect : MonoBehaviour
{
    Vector3 EFFECT_POS = new Vector3(900, 500, 0);
    int EFFECT_DELTE_COUNT = 60;

    private GameObject thunderPrefab_;
    public GameObject firePrefab_;

    GameObject fireEffect_;
    GameObject thunderEffect_;

    GameObject panel;

    bool isEffectPlay_ = false;
    int effecFrameCount_ = 0;

    // Start is called before the first frame update
    void Start()
    {
        panel = GameObject.Find("Panel");
    }

    // Update is called once per frame
    void Update()
    {
        if (isEffectPlay_)
        {
            effecFrameCount_++;
        }

        if (effecFrameCount_ >= EFFECT_DELTE_COUNT)
        {
            panel.GetComponent<PanelManager>().Panel_OFF();

            isEffectPlay_ = false;
            Destroy(fireEffect_);
            Destroy(thunderEffect_);
        }
    }

    public void SkillEffectPlay()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) && WeaponCursor.weapon_ == (int)WEAPON_TYPE.LAEVATEINN && isEffectPlay_ == false)
        {
            //炎EFFECTを作成
            fireEffect_ = Instantiate(firePrefab_) as GameObject;
            fireEffect_.transform.SetParent(GameObject.Find("particlCanvas").transform);
            fireEffect_.transform.position = EFFECT_POS;

            panel.GetComponent<PanelManager>().Panel_ON();

            isEffectPlay_ = true;
            effecFrameCount_ = 0;

            
        }


        if (Input.GetKeyDown(KeyCode.KeypadEnter) && WeaponCursor.weapon_ == (int)WEAPON_TYPE.MJOLNIR && isEffectPlay_ == false)
        {
            //雷エフェクトを作成
            thunderEffect_ = Instantiate((GameObject)Resources.Load("Skill_T")) as GameObject;
            thunderEffect_.transform.SetParent(GameObject.Find("particlCanvas").transform);
            thunderEffect_.transform.position = EFFECT_POS;

            panel.GetComponent<PanelManager>().Panel_ON();

            isEffectPlay_ = true;
            effecFrameCount_ = 0;
        }
    }
}
