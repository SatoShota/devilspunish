﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TitleSceneManager : MonoBehaviour
{
    GameObject TitleAnime;

    //BGMオブジェクト
    GameObject audioBgm_;

    //SEオブジェクト
    GameObject audioSe_;

    // Start is called before the first frame update
    void Start()
    {
        TitleAnime = GameObject.Find("TitleAnimeManager");

        //BGMオブジェクトを探す
        audioBgm_ = GameObject.Find("BackGroundMusic");

        //タイトルシーンのBGMを流す
        audioBgm_.GetComponent<BackGroundMusic>().ActBgm();

    }

    // Update is called once per frame
    void Update()
    {
        //動き５になっている時且つ、エンターキーを押されたら
        if(TitleAnime.GetComponent<TItleAnimeManager>().GetMoveState() == (int)MOVE_STATE.MOVE_5 &&
            Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            //SEオブジェクトを探す
            audioSe_ = GameObject.Find("SoundEffect");

            //決定音を流す    
            audioSe_.GetComponent<SoundEffect>().SelectEnter();

            SceneManager.LoadScene("PlayScene");
        }
    }
    
}
