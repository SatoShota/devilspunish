﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//悪魔たち
enum DEVILS
    {
        EMPTY,
        DEVIL,
        EVILGOD
    }

public class Stage : MonoBehaviour
{
    //雲オブジェクト
    GameObject cloud1_;
    GameObject cloud2_;
    GameObject cloud3_;
    GameObject cloud4_;
    GameObject cloud5_;
    GameObject cloud6_;
    GameObject cloud7_;
    GameObject cloud8_;
    GameObject cloud9_;

    //新しい雲オブジェクト
    GameObject cloud2_1;
    GameObject cloud2_2;
    GameObject cloud2_3;
    GameObject cloud2_4;

    //雲表示座標
    private Vector3 SPAWN1_ = new Vector3(-240, 300, 0);
    private Vector3 SPAWN2_ = new Vector3(0, 300, 0);
    private Vector3 SPAWN3_ = new Vector3(240, 300, 0);
    private Vector3 SPAWN4_ = new Vector3(-240, 0, 0);
    private Vector3 SPAWN5_ = new Vector3(0, 0, 0);
    private Vector3 SPAWN6_ = new Vector3(240, 0, 0);
    private Vector3 SPAWN7_ = new Vector3(-240, -300, 0);
    private Vector3 SPAWN8_ = new Vector3(0, -300, 0);
    private Vector3 SPAWN9_ = new Vector3(240, -300, 0);

    //新しい雲座標
    private Vector3 SPAWN2_1 = new Vector3(0, -200, 0);
    private Vector3 SPAWN2_2 = new Vector3(0, 100, 0);
    private Vector3 SPAWN2_3 = new Vector3(0, -500, 0);
    private Vector3 SPAWN2_4 = new Vector3(0, 400, 0);

    private const int END_TIME = 180;

    //ステージの最大数
    const int MAP_MAX = 10;

    //スコアオブジェクト
    GameObject score_;

    //プレイヤーオブジェクト
    GameObject player_;

    //BGMオブジェクト
    GameObject audioBgm_;

    //効果音オブジェクト
    GameObject soundEffect_;

    //デビルたち格納用配列
    int[] devils_ = new int[10];

    //スキルによる死亡位置格納
    List<int> delete_ = new List<int> { 0 };

    //エフェクトが作成されている位置格納用
    List<bool> effectFlag_ = new List<bool> { false };

    int[] effectCnt_ = new int[10];

    //消える位置
    int deletePos_ = 0;

    //倒した悪魔たちの数
    private int devilCnt_ = 0;
    private int evilgodCnt_ = 0;

    //ゲーム終了してからのカウント
    private int gameendCnt_ = 0;
    private bool gameend_ = false;

    // Start is called before the first frame update
    void Start()
    {
        //魔法
        Application.targetFrameRate = 60;

        //雲生成
        //CreateCloud();
        NewCreateCloud();

        //リストクリア
        delete_.Clear();

        //プレイヤーオブジェクト検索
        player_ = GameObject.Find("Player");

        //スコアオブジェクト検索
        score_ = GameObject.Find("ScoreManager");

        //ゲーム開始時に各オブジェクトを検索してもらう
        player_.GetComponent<Player>().Init();

        //プレイシーンBGMの再生
        audioBgm_ = GameObject.Find("BackGroundMusic");
        audioBgm_.GetComponent<BackGroundMusic>().ActBgm();

        soundEffect_ = GameObject.Find("SoundEffect");

        //初期化
        for (int i = 0; i < MAP_MAX; i++)
        {
            devils_[i] = (int)DEVILS.EMPTY;
            effectFlag_.Add(false);
            effectCnt_[i] = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //ゲーム終了していたら
        if(gameend_)
        {
            gameendCnt_++;
        }

        //終了してから3秒経ったら
        if(gameendCnt_ > END_TIME)
        {
            //リザルトシーンへ移行
            SceneManager.LoadScene("ResultScene");
        }

        EffectCount();

        CheckEffect();
    }

    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void ImageLoad(GameObject workImage, string fileName, Vector3 pos,string canvasName)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find(canvasName).transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

    private void CreateCloud()
    {
        cloud1_ = new GameObject("Cloud1");
        ImageLoad(cloud1_, "Cloud_2", SPAWN1_,"Canvas");

        cloud2_ = new GameObject("Cloud2");
        ImageLoad(cloud2_, "Cloud_2", SPAWN2_,"Canvas");

        cloud3_ = new GameObject("Cloud3");
        ImageLoad(cloud3_, "Cloud_2", SPAWN3_, "Canvas");

        cloud4_ = new GameObject("Cloud4");
        ImageLoad(cloud4_, "Cloud_2", SPAWN4_, "Canvas");

        cloud5_ = new GameObject("Cloud5");
        ImageLoad(cloud5_, "Cloud_2", SPAWN5_, "Canvas");

        cloud6_ = new GameObject("Cloud6");
        ImageLoad(cloud6_, "Cloud_2", SPAWN6_, "Canvas");

        cloud7_ = new GameObject("Cloud7");
        ImageLoad(cloud7_, "Cloud_2", SPAWN7_, "Canvas");

        cloud8_ = new GameObject("Cloud8");
        ImageLoad(cloud8_, "Cloud_2", SPAWN8_, "Canvas");

        cloud9_ = new GameObject("Cloud9");
        ImageLoad(cloud9_, "Cloud_2", SPAWN9_, "Canvas");
    }

    private void NewCreateCloud()
    {
        cloud2_1 = new GameObject("Cloud10");
        ImageLoad(cloud2_1, "Cloud_", SPAWN2_1, "CloudCanvas2");

        cloud2_2 = new GameObject("Cloud11");
        ImageLoad(cloud2_2, "Cloud_", SPAWN2_2, "CloudCanvas1");

        cloud2_3 = new GameObject("Cloud12");
        ImageLoad(cloud2_3, "Cloud_", SPAWN2_3, "CloudCanvas3");

        cloud2_4 = new GameObject("Cloud13");
        ImageLoad(cloud2_4, "Cloud_", SPAWN2_4, "Canvas");
    }

    //生成した悪魔の種類を格納
    public void SetDevils(int pos, int index)
    {
        devils_[pos] = index;
    }

    //デビル死亡時に呼び出される要素リセット関数
    public void ClearDevils(int pos, bool flag)
    {
        devils_[pos] = (int)DEVILS.EMPTY;
        deletePos_ = 0;
    }

    //スキルによる悪魔消去
    public void SkillClear(int pos, int num)
    {
        //クリア
        devils_[pos] = (int)DEVILS.EMPTY;
        delete_.RemoveAt(num);
    }

    //生成したい場所に何もいないかをチェック   
    public bool CheckSpawn(int pos)
    {
        if (devils_[pos] == (int)DEVILS.EMPTY)
        {
            return true;
        }

        return false;
    }

    //そのマスに何がいるかを通知
    public int GetDevils(int index)
    {
        return devils_[index];
    }

    //攻撃されたマスを通知してもらう
    public void SetDeletePos(int pos)
    {
        deletePos_ = pos;
    }

    //どこの誰が死んだかを通知
    public int SendDeleteDevils()
    {
        return deletePos_;
    }

    //スキルにより死んだ場所を通知
    public List<int> SendSkillDelete()
    {
        return delete_;
    }

    //スキルによる攻撃処理
    public void SkillAllDelete()
    {
        int devil = 0;
        int evil = 0;

        //スキル使用時の効果音を呼ぶ
        soundEffect_.GetComponent<SoundEffect>().ActSe(true);

        for(int i = 0; i < MAP_MAX; i++)
        {

            //穴にいるのが何かを判定
            switch(devils_[i])
            {
                //悪魔だった時
                case (int)DEVILS.DEVIL:

                    //悪魔の数を1増やす
                    devil++;

                    //Destroy(GameObject.Find("Devil" + i).gameObject);

                    GameObject devilObj = GameObject.Find("Devil" + i);

                    devilObj.GetComponent<Devil>().SetKillFlag();

                    devilObj.GetComponent<Devil>().SetSkillFlag();

                    delete_.Add(i);

                    break;
 
                //邪神だった時
                case (int)DEVILS.EVILGOD:

                    //邪神の数を1増やす
                    evil++;

                    //Destroy(GameObject.Find("Evil" + i).gameObject);

                    GameObject evilObj = GameObject.Find("Evil" + i);

                    evilObj.GetComponent<EvilGod>().SetKillFlag();

                    evilObj.GetComponent<EvilGod>().SetSkillFlag();

                    delete_.Add(i);

                    break;
            }
        }

        devilCnt_ += devil;
        evilgodCnt_ += evil;

        //プレイヤーに何を何体倒したかを通知
        //player_.GetComponent<Player>().SetKillDevilCnt(devil, evil);

        //スコアに何を何体倒したかを通知
        score_.GetComponent<SaveSample>().SetSkillScore(devil, evil);
    }

    //エフェクト作成
    private void CreateEffects(GameObject effect,int index)
    {
        switch(index)
        {
            case 1:
                ImageLoad(effect, "sonic", SPAWN1_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 2:
                ImageLoad(effect, "sonic", SPAWN2_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 3:
                ImageLoad(effect, "sonic", SPAWN3_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 4:
                ImageLoad(effect, "sonic", SPAWN4_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 5:
                ImageLoad(effect, "sonic", SPAWN5_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 6:
                ImageLoad(effect, "sonic", SPAWN6_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 7:
                ImageLoad(effect, "sonic", SPAWN7_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 8:
                ImageLoad(effect, "sonic", SPAWN8_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
            case 9:
                ImageLoad(effect, "sonic", SPAWN9_, "CloudCanvas3");
                effectFlag_[index] = true;
                break;
        }
    }


    //一定時間エフェクトを　出すためのカウント関数
    private void EffectCount()
    {
        for(int i = 0; i < MAP_MAX; i++)
        {
            if(effectFlag_[i] == true)
            {
                effectCnt_[i]++;
            }

            if(effectCnt_[i] > 20)
            {
                Destroy(GameObject.Find("Sonic" + i).gameObject);
                effectCnt_[i] = 0;
                effectFlag_[i] = false;
            }
        }
    }


    //外部のクラスからエフェクトの表示を通知してもらう関数
    public void AddEffect(int pos)
    {
        //エフェクトを生成
        GameObject effect = new GameObject("Sonic" + pos);

        CreateEffects(effect, pos);

        KillCount(pos);

        soundEffect_.GetComponent<SoundEffect>().ActSe();
    }

    //倒した悪魔の数を渡す
    public int GetKillDevil()
    {
        return devilCnt_;
    }

    //倒した邪神の数を渡す
    public int GetKillEvilgod()
    {
        return evilgodCnt_;
    }

    //キル数を数える
    private void KillCount(int pos)
    {
        switch(devils_[pos])
        {
            case (int)DEVILS.DEVIL:
                devilCnt_++;
                break;
            case (int)DEVILS.EVILGOD:
                evilgodCnt_++;
                break;
        }
    }

    //エフェクトが残らないようチェック
    private void CheckEffect()
    {
        bool flag = false;
        for (int i = 0; i < MAP_MAX; i++)
        {
            
            if(effectFlag_[i])
            {
                flag = true;
            }

            if(flag)
            {
                break;
            }
        }

        if(flag == false)
        {
            for (int i = 1; i < MAP_MAX; i++)
            {
                if (GameObject.Find("Sonic" + i) != null)
                {
                    Destroy(GameObject.Find("Sonic" + i).gameObject);
                }
            }
        }
    }

    //ゲーム終了画像表示
    public void AddGameSet()
    {
        GameObject gameset = new GameObject("GameSet");
        ImageLoad(gameset, "GameSet", new Vector3(0, 0, 0), "CloudCanvas3");
        gameend_ = true;
    }
}
