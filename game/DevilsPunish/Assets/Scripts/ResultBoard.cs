﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultBoard : MonoBehaviour
{
    //positionの位置（一応とっておく）
    //private const float LEAVATEINN_POS_X = 300.0f;
    //private const float DEVIL_POS_X = 1100.0f;
    //private const float EVIL_GOD_POS_X = 1460.0f;
    //private const float SCORE_POS_X = 700.0f;
    //private const float DEVIL_KILL_POS_X = 1280.0f;
    //private const float EVIL_GOD_KILL_POS_X = 1640.0f;
    //private const float NORMAL_POS_Y = 0.0f;
    //private const float SCORE_POS_Y = 525.0f;
    //private const float COUNT_POS_Y = 490.0f;

    ///////////////// localPositionの位置 /////////////////
    //アイコンオブジェクトのY位置
    private const float LEAVATEINN_POS_X = -660.0f;
    private const float DEVIL_POS_X = 140.0f;
    private const float EVIL_GOD_POS_X = 440.0f;

    //テキストオブジェクトのY位置
    private const float SCORE_POS_X = -260.0f;
    private const float DEVIL_KILL_POS_X = 290.0f;
    private const float EVIL_GOD_KILL_POS_X = 590.0f;
    private const float RANK_POS_X = -600.0f;

    //Y位置が真ん中のオブジェクトのY位置
    private const float NORMAL_POS_Y = 0.0f;

    //Y位置が真ん中ではない各種オブジェクトのY位置
    private const float SCORE_POS_Y = 0.5f;
    private const float COUNT_POS_Y = -30.0f;

    ///////////////// localPositionの位置（変化後） /////////////////
    //アイコンオブジェクトのY位置
    private const float CHANGED_LEAVATEINN_POS_X = -450.0f;
    private const float CHANGED_DEVIL_POS_X = 150.0f;
    private const float CHANGED_EVIL_GOD_POS_X = 390.0f;

    //テキストオブジェクトのY位置
    private const float CHANGED_SCORE_POS_X = -150.0f;
    private const float CHANGED_DEVIL_KILL_POS_X = 270.0f;
    private const float CHANGED_EVIL_GOD_KILL_POS_X = 510.0f;

    //Y位置が真ん中ではない各種オブジェクトのY位置
    private const float CHANGED_SCORE_POS_Y = 0.0f;
    private const float CHANGED_COUNT_POS_Y = -35.0f;

    //サイズが変化するオブジェクトの初期状態のサイズ
    private const float INIT_SCORE_SCALE = 1.8f;
    private const float INIT_COUNT_SCALE = 1.5f;
    private const float INIT_RANK_SCALE = 1.5f;

    //サイズ変更後のサイズ
    private const float CHANGED_SCORE_SCALE = 1.3f;
    private const float CHANGED_COUNT_SCALE = 0.7f;

    //武器名
    private const string LAEVATEINN = "Laevateinn";
    private const string MJOLNIR = "Mjolnir";

    //今回のデータの背景
    public GameObject currentDataBackGround_ = null;

    //レーヴァテインアイコン
    public GameObject laevateinnIcon_ = null;

    //ミョルニルアイコン
    public GameObject mjolnirIcon_ = null;

    //悪魔アイコン
    public GameObject devilIcon_ = null;

    //邪神アイコン
    public GameObject evilGodIcon_ = null;

    //スコアを表示するオブジェクト
    public GameObject scoreBoard_ = null;

    //悪魔を倒した数を表示するオブジェクト
    public GameObject devilCountBoard_ = null;

    //邪神を倒した数を表示するオブジェクト
    public GameObject evilGodCountBoard_ = null;

    //邪神を倒した数を表示するオブジェクト
    public GameObject rankBoard_ = null;

    //使用している武器
    private GameObject weaponIcon_ = null;

    //武器名
    private string weaponName_ = "";

    //スコア
    private int score_ = 0;

    //悪魔を倒した数
    private int devilNum_ = 0;

    //邪神を倒した数
    private int evilGodNum_ = 0;

    //順位
    private int rank_ = 0;

    //指定の位置まで動くかどうかのフラグ
    private bool moveFlag_ = false;

    //X方向に動く値
    private float movePosX_ = -30.0f;

    //数字（フォント）の色
    private Color numberColor_;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        //動かすべきなら
        if(moveFlag_)
        {
            //ボードを動かす
            MoveBoard();
        }
    }

    //今回のデータならば背景を作成
    public void CreateBackGround()
    {
        currentDataBackGround_ = CreateIconObject(currentDataBackGround_);

        currentDataBackGround_.GetComponent<RectTransform>().localPosition =
            new Vector3(0.0f, 0.0f, 0.0f);
    }

    //中身を作り出す
    //　※ここではアイコンのみを作成
    public void CreateContents()
    {
        ////////////////////////// 武器生成 //////////////////////////
        weaponIcon_ = CreateWeaponIcon(weaponName_);
        
        weaponIcon_.GetComponent<RectTransform>().localPosition =
            new Vector3(LEAVATEINN_POS_X, NORMAL_POS_Y, weaponIcon_.GetComponent<RectTransform>().localPosition.z);


        ////////////////////////// 悪魔アイコン生成 //////////////////////////
        devilIcon_ = CreateIconObject(devilIcon_);
        
        devilIcon_.GetComponent<RectTransform>().localPosition =
            new Vector3(DEVIL_POS_X, NORMAL_POS_Y, devilIcon_.GetComponent<RectTransform>().localPosition.z);


        ////////////////////////// 邪神アイコン生成 //////////////////////////
        evilGodIcon_ = CreateIconObject(evilGodIcon_);
        
        evilGodIcon_.GetComponent<RectTransform>().localPosition =
            new Vector3(EVIL_GOD_POS_X, NORMAL_POS_Y, evilGodIcon_.GetComponent<RectTransform>().localPosition.z);
    }

    //テキストのオブジェクトを追加（この関数の中で生成）する
    //　※遅れて生成するときに必要だったので作成しました。
    public void AddTextObject()
    {
        ////////////////////////// スコア生成 //////////////////////////
        scoreBoard_ = CreateTextObject(scoreBoard_);
        
        scoreBoard_.GetComponent<RectTransform>().localPosition =
            new Vector3(SCORE_POS_X, SCORE_POS_Y, scoreBoard_.GetComponent<RectTransform>().localPosition.z);
        
        scoreBoard_.GetComponent<Text>().text = score_.ToString();

        scoreBoard_.GetComponent<RectTransform>().localScale =
            new Vector3(INIT_SCORE_SCALE, INIT_SCORE_SCALE, 1.0f);


        ////////////////////////// 悪魔を倒した数を表示するオブジェクトを生成 //////////////////////////
        devilCountBoard_ = CreateTextObject(devilCountBoard_);
        
        devilCountBoard_.GetComponent<RectTransform>().localPosition =
            new Vector3(DEVIL_KILL_POS_X, COUNT_POS_Y, devilCountBoard_.GetComponent<RectTransform>().localPosition.z);
        
        devilCountBoard_.GetComponent<Text>().text = "x" + devilNum_.ToString();


        ////////////////////////// 邪神を倒した数を表示するオブジェクトを生成 //////////////////////////
        evilGodCountBoard_ = CreateTextObject(evilGodCountBoard_);

        evilGodCountBoard_.GetComponent<RectTransform>().localPosition =
            new Vector3(EVIL_GOD_KILL_POS_X, COUNT_POS_Y, evilGodCountBoard_.GetComponent<RectTransform>().localPosition.z);
        
        evilGodCountBoard_.GetComponent<Text>().text = "x" + evilGodNum_.ToString();
    }

    //順位のテキストオブジェクトを作成する
    public void CreateRankText()
    {
        rankBoard_ = CreateTextObject(rankBoard_);

        rankBoard_.GetComponent<RectTransform>().localPosition =
            new Vector3(RANK_POS_X, NORMAL_POS_Y, rankBoard_.GetComponent<RectTransform>().localPosition.z);
        
        rankBoard_.GetComponent<Text>().text = rank_.ToString();

        //サイズ調整
        rankBoard_.GetComponent<RectTransform>().localScale = new Vector3(INIT_RANK_SCALE, INIT_RANK_SCALE, 1.0f);
    }


    //武器を生成する
    public GameObject CreateWeaponIcon(string weaponName)
    {
        //引数の武器名を確認し、該当する武器を格納
        switch (weaponName)
        {
            case LAEVATEINN:
                return CreateIconObject(laevateinnIcon_);

            case MJOLNIR:
                return CreateIconObject(mjolnirIcon_);

            default:
                return CreateIconObject(laevateinnIcon_);
        }
    }

    //オブジェクトを生成する
    public GameObject CreateIconObject(GameObject obj)
    {
        //インスタンスを生成
        obj = (GameObject)Instantiate(obj) as GameObject;

        //親を設定（キャンバス）
        obj.transform.SetParent(this.gameObject.transform);

        //アンカーポジションの設定
        obj.GetComponent<RectTransform>().anchoredPosition = new Vector3(0.0f, 0.0f, 0.0f);

        //縮尺を問う等倍にする
        obj.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

        //アスペクト比をもとのサイズ比
        obj.GetComponent<Image>().preserveAspect = true;

        //サイズを元画像のサイズにする
        obj.GetComponent<Image>().SetNativeSize();

        //値渡しなので返り値とする
        return obj;
    }

    //テキストオブジェクトを生成する
    public GameObject CreateTextObject(GameObject obj)
    {
        //インスタンスを生成
        obj = (GameObject)Instantiate(obj) as GameObject;

        //親を設定（キャンバス）
        obj.transform.SetParent(this.gameObject.transform);

        //アンカーポジションの設定
        obj.GetComponent<RectTransform>().anchoredPosition = new Vector3(0.0f, 0.0f, 0.0f);

        //縮尺を等倍にする
        obj.GetComponent<RectTransform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);

        return obj;
    }

    //表示している内容のサイズや配置場所などを変える
    public void MoveContents()
    {
        //順位を作成
        CreateRankText();

        //武器を移動
        weaponIcon_.GetComponent<RectTransform>().localPosition = ChangePosition(
            weaponIcon_.GetComponent<RectTransform>().localPosition, CHANGED_LEAVATEINN_POS_X, NORMAL_POS_Y);

        //スコアボードを移動
        scoreBoard_.GetComponent<RectTransform>().localPosition = ChangePosition(
            scoreBoard_.GetComponent<RectTransform>().localPosition, CHANGED_SCORE_POS_X, CHANGED_SCORE_POS_Y);

        //サイズ調整
        scoreBoard_.GetComponent<RectTransform>().localScale =
            new Vector3(CHANGED_SCORE_SCALE, CHANGED_SCORE_SCALE, 1.0f);

        //悪魔アイコンを移動
        devilIcon_.GetComponent<RectTransform>().localPosition = ChangePosition(
            devilIcon_.GetComponent<RectTransform>().localPosition, CHANGED_DEVIL_POS_X, NORMAL_POS_Y);

        //悪魔撃破数を移動
        devilCountBoard_.GetComponent<RectTransform>().localPosition = ChangePosition(
            devilCountBoard_.GetComponent<RectTransform>().localPosition, CHANGED_DEVIL_KILL_POS_X, CHANGED_COUNT_POS_Y);

        //サイズを調整
        devilCountBoard_.GetComponent<RectTransform>().localScale =
            new Vector3(CHANGED_COUNT_SCALE, CHANGED_COUNT_SCALE, 1.0f);

        //邪神アイコンを移動
        evilGodIcon_.GetComponent<RectTransform>().localPosition = ChangePosition(
            evilGodIcon_.GetComponent<RectTransform>().localPosition, CHANGED_EVIL_GOD_POS_X, NORMAL_POS_Y);

        //邪神撃破数を移動
        evilGodCountBoard_.GetComponent<RectTransform>().localPosition = ChangePosition(
            evilGodCountBoard_.GetComponent<RectTransform>().localPosition, CHANGED_EVIL_GOD_KILL_POS_X, CHANGED_COUNT_POS_Y);

        //サイズを調整
        evilGodCountBoard_.GetComponent<RectTransform>().localScale =
            new Vector3(CHANGED_COUNT_SCALE, CHANGED_COUNT_SCALE, 1.0f);
    }

    //ボード自体を動かす
    public void MoveBoard()
    {
        //原点の位置に戻ってくるまで動く
        if(this.gameObject.GetComponent<RectTransform>().localPosition.x >= 0.0f)
        {
            this.gameObject.GetComponent<RectTransform>().localPosition +=
                new Vector3(movePosX_, 0.0f, 0.0f);
        }
        else
        {
            moveFlag_ = false;
        }
    }

    //引数でもらった位置ベクトルを、第二・三引数で指定された位置に変えて返す
    public Vector3 ChangePosition(Vector3 vec, float posX, float posY)
    {
        vec = new Vector3(posX, posY, 0.0f);
        return vec;
    }

    //武器名を設定する
    public void SetWeaponName(string name)
    {
        weaponName_ = name;
    }

    //スコアを設定する
    public void SetScore(int score)
    {
        score_ = score;
    }

    //敵を倒した数をもらう
    public void SetDevilKillCount(int killCount)
    {
        devilNum_ = killCount;
    }

    //邪神を倒した数をもらう
    public void SetEvilGodCount(int evilGodCount)
    {
        evilGodNum_ = evilGodCount;
    }

    //順位を設定する
    public void SetRank(int rank)
    {
        rank_ = rank;
    }

    //スコアを渡す
    public int GetScore()
    {
        return score_;
    }

    //ボードが動くかどうかのフラグを変化させる
    public void MoveFlagOn()
    {
        moveFlag_ = true;
    }

    //数字（フォント）の色を設定
    public void SetNumberColor(Color newColor)
    {
        scoreBoard_.GetComponent<Text>().color = newColor;
        devilCountBoard_.GetComponent<Text>().color = newColor;
        evilGodCountBoard_.GetComponent<Text>().color = newColor;
        rankBoard_.GetComponent<Text>().color = newColor;
    }
}
