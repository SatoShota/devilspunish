﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class Devil : MonoBehaviour
{
    //上下移動に要するフレーム数の最大
    private const int MOVE_FLAME = 90;

    //フレームカウント用
    int countFlame_ = 0;

    //上に上がり切ったかを判定するフラグ
    bool upFlag_ = false;

    //生成時の位置格納用
    Vector3 resPos_;

    //ステージオブジェクト受け取り
    GameObject stage_;

    //自分が死ぬかどうか
    bool killFlag_ = false;

    //スキルにより死んだかどうか
    bool skillFlag_ = false;

    bool animFlag_ = false;

    //自分の番号記憶用
    int indexNum_ = 0;

    //死へのカウントダウン
    int killCountDown_ = 0;

    int clearIndex_ = 0;

    

    // Start is called before the first frame update
    void Start()
    {
        stage_ = GameObject.Find("StageManager");

        resPos_ = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //移動処理
        MoveDevil();

        //プレイヤーが殺しに来たら
        if(stage_.GetComponent<Stage>().SendDeleteDevils() == indexNum_)
        {
            if(killFlag_ == false)
            {
                //叩かれたエフェクトを出す
                stage_.GetComponent<Stage>().AddEffect(indexNum_);
            }

            //自分が死んだことを理解
            killFlag_ = true;

            //やられアニメーションに変更
            this.gameObject.GetComponent<Anime>().SetAnimeState(true);
        }

        if (killFlag_ == true && animFlag_ == false)
        {
            animFlag_ = true;

            //やられアニメーションに変更
            this.gameObject.GetComponent<Anime>().SetAnimeState(true);
        }

        //死亡が確定したら
        if (killFlag_)
        {
            killCountDown_++;
        }

        //20フレームの間やられアニメーションを見せるため死なない
        if (killCountDown_ > 20)
        {
            //もしスキルにより殺されていたら
            if (skillFlag_)
            {
                //消滅時、ステージ側にいなくなったことを通知
                stage_.GetComponent<Stage>().SkillClear(indexNum_, clearIndex_);
                Destroy(this.gameObject);
            }
            else
            {
                //消滅時、ステージ側にいなくなったことを通知
                stage_.GetComponent<Stage>().ClearDevils(indexNum_, true);
                Destroy(this.gameObject);
            }

        }
    }

    //悪魔の移動処理
    private void MoveDevil()
    {

        //上昇処理
        if(countFlame_ < MOVE_FLAME && upFlag_ == false)
        {
            countFlame_++;
            Vector3 pos = transform.position;
            pos.y += 3.0f;
            transform.position = pos;
        }
        else
        {
            upFlag_ = true;
            countFlame_ = 0;
        }

        //下降処理
        if (upFlag_ == true && countFlame_ < MOVE_FLAME)
        {
            countFlame_++;
            Vector3 pos = transform.position;
            pos.y -= 3.0f;
            transform.position = pos;
        }

        //下限まで降りたら消滅
        if (transform.position.y < resPos_.y && killFlag_ == false)
        {
            //消滅時、ステージ側にいなくなったことを通知
            stage_.GetComponent<Stage>().ClearDevils(indexNum_, false);
            Destroy(this.gameObject);
        }

    }

    //自分の番号を通知してもらう関数
    public void SetIndex(int index)
    {
        indexNum_ = index;
    }

    public void SetKillFlag()
    {
        killFlag_ = true;
    }

    public void SetSkillFlag()
    {
        skillFlag_ = true;
    }

    public bool GetKill()
    {
        return killFlag_;
    }
}

