﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    //最大時間
    private const int TIME_MAX = 30;

    //最大フレーム
    private const int FLAME_MAX = 60;

    private const int TEN_SEC = 10;

    //フレームカウント
    private int flameCount_ = 0;

    //フレームカウントインデックス
    private int flameIndex_ = 0;

    //タイマーテキスト
    private Text timerText_;

    //分表示用変数
    private int min_ = 0;

    //秒表示用変数
    private int sec1_ = 3;
    private int sec2_ = 0;

    //タイマーオブジェクト取得
    GameObject timer_ = null;

    //プレイヤーオブジェクト取得
    GameObject player_ = null;

    //ステージオブジェクト取得
    GameObject stage_ = null;

    //開始フラグ
    bool start_ = false; 

    // Start is called before the first frame update
    void Start()
    {
        //タイマーテキストを探す
        timer_ = GameObject.Find("Timer");

        //テキストコンポーネント取得
        timerText_ = timer_.GetComponent<Text>();

        //プレイヤー検索
        player_ = GameObject.Find("Player");

        //ステージ検索
        stage_ = GameObject.Find("StageManager");
    }


    // Update is called once per frame
    void Update()
    {

        //分と秒が0になったらリザルトへ移動
        if (min_ == 0 && sec1_ == 0 && sec2_ == 0)
        {
            player_.GetComponent<Player>().ResetStartFlag();
            if (start_)
            {
                stage_.GetComponent<Stage>().AddGameSet();
            }
            start_ = false;
        }

        if (start_ == true)
        {           

            //60フレームカウント
            if (flameCount_ < FLAME_MAX)
            {
                flameCount_++;
            }
            else
            {
                //1の位が0になったら
                if(sec1_ != 0 && sec2_ == 0)
                {
                    sec1_--;
                    flameIndex_ = 0;
                }

                flameIndex_++;
                sec2_ = TEN_SEC - flameIndex_;
                flameCount_ = 0;
            }


            //テキスト表示をソースのものに置き換え
            timerText_.text = min_ + ":" + sec1_ + sec2_;
        }
    }

    public void SetStartFlag()
    {
        start_ = true;
    }
}
