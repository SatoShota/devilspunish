﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundEffect : MonoBehaviour
{
    //各シーン名
    private const string TITLE_SCENE = "TitleScene";
    private const string PLAY_SCENE = "PlayScene";
    private const string RESULT_SCENE = "ResultScene";
    private const float DELAY_TIME = 2.5f;

    //選択武器
    private int weapon_ = WeaponCursor.weapon_;

    //武器が交差したタイミングで流す効果音
    public AudioClip weaponCrossSe_;

    //武器選択時の効果音と決定音
    public AudioClip titleSelectSe_;
    public AudioClip titleSelectEnterSe_;

    //プレイシーンの攻撃とスキルのBGM
    public AudioClip playAttackSe_;
    public AudioClip playSkillSe1_;
    public AudioClip playSkillSe2_;
    public AudioClip playSkillMax_;
    public AudioClip playQuestReward_;

    //スコア表示待ち時間のドラムロール
    public AudioClip resultDrumRoll_;

    //オーディオクラスオブジェクト(これを使ってSEを流す)
    AudioSource audioSource_;

    //一番最初の自分のオブジェクトを記憶するためのオブジェクト
    public static SoundEffect instance_;

    //スタートより先に自分が死なないオブジェクトとして登録
    private void Awake()
    {
        if (instance_ == null)
        {
            instance_ = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //Componentを取得
        audioSource_ = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
    }

    //効果音呼び出し関数
    public void ActSe(bool skill = false)
    {

        //各シーンによって流す効果音を変更
        switch (SceneManager.GetActiveScene().name)
        {
            case TITLE_SCENE:

                //武器選択時に呼ぶ
                audioSource_.PlayOneShot(titleSelectSe_);

                break;

            case PLAY_SCENE:

                if(skill)
                {
                    if (weapon_ == (int)WEAPON_TYPE.LAEVATEINN)
                    {
                        //スキル発動時
                        audioSource_.PlayOneShot(playSkillSe1_);
                    }
                    else
                    {
                        //スキル発動時
                        audioSource_.PlayOneShot(playSkillSe2_);
                    }
                }
                else
                {
                    //攻撃ボタンが押されたとき
                    audioSource_.PlayOneShot(playAttackSe_);
                }

                break;

            case RESULT_SCENE:

                //ランキング表示時
                audioSource_.clip = resultDrumRoll_;    //音源設定
                audioSource_.time = DELAY_TIME;         //再生位置指定
                audioSource_.Play();                    //音源再生
                audioSource_.loop = false;              //ループ不可に設定
                break;
        }
    }

    //武器交差時効果音
    public void CrossSe()
    {
        audioSource_.PlayOneShot(weaponCrossSe_);
    }

    //武器決定時効果音
    public void SelectEnter()
    {
        audioSource_.PlayOneShot(titleSelectEnterSe_);
    }

    //スキルゲージが100％になった時に流す効果音
    public void SkillMax()
    {
        audioSource_.PlayOneShot(playSkillMax_);
    }

    //クエスト達成時効果音
    public void QuestRewardSe()
    {
        audioSource_.PlayOneShot(playQuestReward_);
    }
}
