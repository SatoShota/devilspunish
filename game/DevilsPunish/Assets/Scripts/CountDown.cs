﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    //フレーム最大値
    private const int FLAME_MAX = 60;

    //フレームカウント用変数
    private int flameCount_ = 60;

    //カウントダウンインデックス用変数
    private int flameIndex_ = 4;

    //カウントダウンオブジェクト作成
    GameObject CountDown_;

    // Start is called before the first frame update
    void Start()
    {
        CountDown_ = new GameObject("CountDown");

        Application.targetFrameRate = 60;
    }

    // Update is called once per frame
    void Update()
    {
        if (flameIndex_ > -1)
        {
            //60フレームカウント
            if (flameCount_ < FLAME_MAX)
            {
                flameCount_++;
            }
            else
            {
                Destroy(CountDown_.gameObject);

                flameIndex_ = flameIndex_ - 1;
                flameCount_ = 0;

                CountDown_ = new GameObject("CountDown");
                ImageLoad(CountDown_, "countdown_" + flameIndex_, new Vector3(0, 0, 0));
            }
        }
        else
        {
            //タイマーオブジェクトを検索
            GameObject timer = GameObject.Find("TimerManager");

            //タイマー開始を通知
            timer.GetComponent<Timer>().SetStartFlag();

            //スコアオブジェクトを検索
            GameObject score = GameObject.Find("ScoreManager");

            //スコアカウント開始を通知
            score.GetComponent<SaveSample>().SetStartFlag();

            //クエストオブジェクトを検索
            GameObject quest = GameObject.Find("QuestManager");

            //クエスト生成開始を通知
            quest.GetComponent<Quest>().SetStartFlag();

            //デビル生成オブジェクトを検索
            GameObject devils = GameObject.Find("DevilManager");

            //デビル生成開始を通知
            devils.GetComponent<DevilsGenerator>().SetStartFlag();

            //プレイヤーオブジェクト検索
            GameObject player = GameObject.Find("Player");

            //ゲーム開始を通知
            player.GetComponent<Player>().SetStartFlag();


            //自分を削除
            Destroy(CountDown_.gameObject);

            Destroy(this.gameObject);
        }
    }

    //画像を動的に呼び出します
    //引数    :   ゲームオブジェクト、画像の名前、出す位置（0,0,0が画面中央）
    //戻り値  :    なし
    private void ImageLoad(GameObject workImage, string fileName, Vector3 pos)
    {
        //作ったゲームオブジェクトをCanvasの子に
        workImage.transform.parent = GameObject.Find("CloudCanvas3").transform;

        //画像の位置(アンカーポジション)
        workImage.AddComponent<RectTransform>().anchoredPosition = pos;

        //縮尺がおかしいのでちゃんと等倍にする
        workImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

        //スプライト画像追加
        workImage.AddComponent<Image>().sprite = Resources.Load<Sprite>(fileName);

        //アスペクト比は元の画像の比率を維持。
        workImage.GetComponent<Image>().preserveAspect = true;

        //画像のサイズを元画像と同じサイズにする。
        workImage.GetComponent<Image>().SetNativeSize();
    }

}
