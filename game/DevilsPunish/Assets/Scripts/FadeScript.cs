﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FadeScript : MonoBehaviour
{
    float alfa;
    float speed = 0.01f;
    float red, green, blue;

    GameObject TitleAnime;

    void Start()
    {
        TitleAnime = GameObject.Find("TitleAnimeManager");

        red = GetComponent<Image>().color.r;
        green = GetComponent<Image>().color.g;
        blue = GetComponent<Image>().color.b;
    }

    void Update()
    {
        if (TitleAnime.GetComponent<TItleAnimeManager>().GetMoveState() == (int)MOVE_STATE.MOVE_3)
        {
            GetComponent<Image>().color = new Color(red, green, blue, alfa);
            alfa += speed;
        }

    
    }
}