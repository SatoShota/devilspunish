﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //悪魔を倒した際得られるスキルポイント
    private const int SKILL_POINT_DEVIL = 20;

    //邪神を倒した際に得られるスキルポイント
    private const int SKILL_POINT_EVIL = 100;

    //倒した悪魔の数記憶用
    private int devilCnt_ = 0;

    //倒した邪神の数記憶用
    private int evilgodCnt_ = 0;

    //自分が持っている武器
    private int weapon_ = WeaponCursor.weapon_;

    //スタート判断用変数
    bool start_ = false;

    //自分の得点
    private int score_ = 0;

    //自分のスキルポイント
    //private int skillPoint_ = 0;

    //スコアオブジェクト
    GameObject scoreObj_;

    //スキルオブジェクト
    GameObject skillObj_;

    //スキルエフェクトオブジェクト
    GameObject skillEffect_;

    //たたく位置
    enum POSITION
    {
        EDGE,
        POS1,
        POS2,
        POS3,
        POS4,
        POS5,
        POS6,
        POS7,
        POS8,
        POS9
    }

    //ステージオブジェクト取得
    GameObject stage_;

    public static Player instance_;

    //スタートより先に自分が死なないオブジェクトとして登録
    private void Awake()
    {
        if (instance_ == null)
        {
            instance_ = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (start_)
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                if (skillObj_.GetComponent<SkillGageManager>().isAbleToSkill())
                {
                    stage_.GetComponent<Stage>().SkillAllDelete();

                    skillEffect_.GetComponent<SkillEffect>().SkillEffectPlay();

                    skillObj_.GetComponent<SkillGageManager>().SkillPointReset();
                }
            }

            //テンキーのいずれかのボタンが押されたら
            switch (Input.inputString)
            {
                case "7":
                    CheckPos((int)POSITION.POS1);
                    break;

                case "8":
                    CheckPos((int)POSITION.POS2);
                    break;

                case "9":
                    CheckPos((int)POSITION.POS3);
                    break;

                case "4":
                    CheckPos((int)POSITION.POS4);
                    break;

                case "5":
                    CheckPos((int)POSITION.POS5);
                    break;

                case "6":
                    CheckPos((int)POSITION.POS6);
                    break;

                case "1":
                    CheckPos((int)POSITION.POS7);
                    break;

                case "2":
                    CheckPos((int)POSITION.POS8);
                    break;

                case "3":
                    CheckPos((int)POSITION.POS9);
                    break;
            }

        }

    }

    //位置チェック用関数
    private void CheckPos(int pos)
    {
        //ステージの穴に何かしらがいたときに
        if(stage_.GetComponent<Stage>().CheckSpawn(pos) == false)
        {
            //穴にいたのが悪魔だったら
            if(stage_.GetComponent<Stage>().GetDevils(pos) == (int)DEVILS.DEVIL)
            {
                //devilCnt_++;

                GameObject devilObj = GameObject.Find("Devil" + pos);

                if (devilObj.GetComponent<Devil>().GetKill() == false)
                {
                    skillObj_.GetComponent<SkillGageManager>().AddSkillPoint(SKILL_POINT_DEVIL);

                    stage_.GetComponent<Stage>().SetDeletePos(pos);

                    scoreObj_.GetComponent<SaveSample>().SetScore(stage_.GetComponent<Stage>().GetDevils(pos));
                }
            }


            //穴にいたのが邪神だったら
            if(stage_.GetComponent<Stage>().GetDevils(pos) == (int)DEVILS.EVILGOD)
            {
                //evilgodCnt_++;

                GameObject evilObj = GameObject.Find("Evil" + pos);

                if (evilObj.GetComponent<EvilGod>().GetKill() == false)
                {

                    skillObj_.GetComponent<SkillGageManager>().AddSkillPoint(SKILL_POINT_EVIL);

                    stage_.GetComponent<Stage>().SetDeletePos(pos);

                    scoreObj_.GetComponent<SaveSample>().SetScore(stage_.GetComponent<Stage>().GetDevils(pos));
                }
            }
        }
    }

    //倒した悪魔の数を渡す
    public int GetKillDevil()
    {
        return devilCnt_;
    }

    //倒した邪神の数を渡す
    public int GetKillEvilgod()
    {
        return evilgodCnt_;
    }

    //現在の獲得スコアを渡す
    public int GetScore()
    {
        return score_;
    }

    //スキルによる討伐数を受け取るよう
    public void SetKillDevilCnt(int devil, int evil)
    {
        devilCnt_ += devil;
        evilgodCnt_ += evil;
    }

    //ゲーム開始を通知してもらう関数
    public void SetStartFlag()
    {
        start_ = true;
    }

    //ゲーム終了を通知してもらう関数
    public void ResetStartFlag()
    {
        score_ = scoreObj_.GetComponent<SaveSample>().GetScore();
        devilCnt_ = stage_.GetComponent<Stage>().GetKillDevil();
        evilgodCnt_ = stage_.GetComponent<Stage>().GetKillEvilgod();
        start_ = false;
    }

    //所持している武器を通知
    public int GetWeapon()
    {
        return weapon_;
    }

    public void Init()
    {
        stage_ = GameObject.Find("StageManager");

        scoreObj_ = GameObject.Find("ScoreManager");

        skillObj_ = GameObject.Find("SkillGageManager");

        skillEffect_ = GameObject.Find("SkillEffect");

        //選択した武器を取得
        weapon_ = WeaponCursor.weapon_;
    }
}
