﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    //武器を保存する時のキー
    private const string WEAPON_KEY = "WEAPON_RANK_";

    //スコアを保存する時のキー
    private const string SCORE_KEY = "SCORE_RANK_";

    //悪魔を倒した数を保存する時のキー
    private const string DEVIL_KILL_KEY = "DEVIL_KILL_RANK_";

    //邪神を倒した数を保存する時のキー
    private const string EVIL_GOD_KILL_KEY = "EVIL_GOD_KILL_RANK_";

    //ランキングの上限（5位）
    private const int LIST_MAX = 6;

    //今回の結果
    private const int CURRENT_RESULT = 5;

    //武器を格納する
    private string[] weaponList_ = new string[LIST_MAX];

    //スコアを格納する
    private int[] scoreList_ = new int[LIST_MAX];

    //悪魔を倒した数を格納する
    private int[] devilKillList_ = new int[LIST_MAX];

    //邪神を倒した数を格納する
    private int[] evilGodKillList_ = new int[LIST_MAX];

    //今回の結果かどうかを確認する
    private bool[] checkList_ = new bool[LIST_MAX];

    //リザルトマネージャーを格納する
    private GameObject resultManager_ = null;

    //ゲームを終了するかを確認するオブジェクトを格納する
    private GameObject checkApplicationOuit_ = null;

    // Start is called before the first frame update
    void Start()
    {
        //リザルトマネージャーを取得
        resultManager_ = GameObject.Find("ResultManager");

        //ゲームを終了するかどうかの確認をするオブジェクトを取得
        checkApplicationOuit_ = GameObject.Find("CheckApplicationQuit");

        for (int i = 0; i < LIST_MAX; i++)
        {
            checkList_[i] = false;
        }

        //ランクの最大値（リストの最大値 - 1）分回す
        for (int i = 0; i < LIST_MAX - 1; i++)
        {
            //それぞれの記録を取得
            if (IsExitData(WEAPON_KEY + (i + 1)))
            {
                weaponList_[i] = PlayerPrefs.GetString(WEAPON_KEY + (i + 1));
            }
            else
            {
                weaponList_[i] = "Laevateinn";
            }

            if (IsExitData(SCORE_KEY + (i + 1)))
            {
                scoreList_[i] = PlayerPrefs.GetInt(SCORE_KEY + (i + 1));
            }
            else
            {
                scoreList_[i] = 0;
            }

            if (IsExitData(DEVIL_KILL_KEY + (i + 1)))
            {
                devilKillList_[i] = PlayerPrefs.GetInt(DEVIL_KILL_KEY + (i + 1));
            }
            else
            {
                devilKillList_[i] = 0;
            }

            if (IsExitData(EVIL_GOD_KILL_KEY + (i + 1)))
            {
                evilGodKillList_[i] = PlayerPrefs.GetInt(EVIL_GOD_KILL_KEY + (i + 1));
            }
            else
            {
                evilGodKillList_[i] = 0;
            }
        }

        //各種データの設定
        SetWeapon();
        SetScore();
        SetDevilCount();
        SetEvilGodCount();

        //今回の結果としてチェックする
        checkList_[CURRENT_RESULT] = true;

        //順位付け
        Ranking();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //データをチェック
    public bool IsExitData(string keyName)
    {
        //引数のキーに対する値があれば
        if (PlayerPrefs.HasKey(keyName))
        {
            return true;
        }
        return false;
    }

    //順位をつける
    public void Ranking()
    {
        //一位から決めていく（そのため、五位は比較することはない）
        for (int i = 0; i < LIST_MAX - 1; i++)
        {
            for (int j = 0; j < LIST_MAX; j++)
            {
                SwapContents(i, j);
            }
        }
    }

    //中身を交換する
    public void SwapContents(int mainIndex, int subIndex)
    {
        //退避用
        string stringWorkSpace = "";
        int numberWorkSpace = 0;
        bool checkWorkSpace = false;

        //今の比較対象より下位のものたちと比較し、比較対象が下位のものより小さいなら
        if (mainIndex < subIndex && scoreList_[mainIndex] < scoreList_[subIndex])
        {
            //武器を交換
            stringWorkSpace = weaponList_[mainIndex];
            weaponList_[mainIndex] = weaponList_[subIndex];
            weaponList_[subIndex] = stringWorkSpace;

            //スコアを交換
            numberWorkSpace = scoreList_[mainIndex];
            scoreList_[mainIndex] = scoreList_[subIndex];
            scoreList_[subIndex] = numberWorkSpace;

            //悪魔撃破数を交換
            numberWorkSpace = devilKillList_[mainIndex];
            devilKillList_[mainIndex] = devilKillList_[subIndex];
            devilKillList_[subIndex] = numberWorkSpace;

            //邪神撃破数を交換
            numberWorkSpace = evilGodKillList_[mainIndex];
            evilGodKillList_[mainIndex] = evilGodKillList_[subIndex];
            evilGodKillList_[subIndex] = numberWorkSpace;

            //今回の結果かどうかの交換
            checkWorkSpace = checkList_[mainIndex];
            checkList_[mainIndex] = checkList_[subIndex];
            checkList_[subIndex] = checkWorkSpace;
        }
    }

    //削除したときの処理
    public void OnDestroy()
    {
        //ゲームを終了しているかどうかの判定をするオブジェクトがいないとき
        if(checkApplicationOuit_ != null)
        {
            //ゲームを終了しないなら
            if (!(checkApplicationOuit_.GetComponent<CheckApplicationQuit>().GetCheckFlag()))
            {
                //１位～５位までの
                for (int i = 0; i < LIST_MAX - 1; i++)
                {
                    //情報を保存
                    PlayerPrefs.SetString(WEAPON_KEY + (i + 1), weaponList_[i]);
                    PlayerPrefs.SetInt(SCORE_KEY + (i + 1), scoreList_[i]);
                    PlayerPrefs.SetInt(DEVIL_KILL_KEY + (i + 1), devilKillList_[i]);
                    PlayerPrefs.SetInt(EVIL_GOD_KILL_KEY + (i + 1), evilGodKillList_[i]);
                    PlayerPrefs.Save();
                }
            }
        }
    }

    //リザルトマネージャーから武器の種類を取得し、該当する武器名を格納する
    public void SetWeapon()
    {
        //リザルトマネージャーから武器の種類を取得
        weaponList_[CURRENT_RESULT] = resultManager_.GetComponent<ResultManager>().GetCurrentWeaponName();
    }

    //リザルトマネージャーからスコアを取得する
    public void SetScore()
    {
        scoreList_[CURRENT_RESULT] = resultManager_.GetComponent<ResultManager>().GetCurrentScore();
    }

    //リザルトマネージャーから悪魔を倒した数を取得する
    public void SetDevilCount()
    {
        devilKillList_[CURRENT_RESULT] = resultManager_.GetComponent<ResultManager>().GetCurrentDevilCount();
    }

    //リザルトマネージャーから邪神を倒した数を取得する
    public void SetEvilGodCount()
    {
        evilGodKillList_[CURRENT_RESULT] = resultManager_.GetComponent<ResultManager>().GetCurrentEvilGodCount();
    }

    //引数で渡された場所の武器名を渡す
    public string GetWeaponName(int index)
    {
        return weaponList_[index];
    }

    //引数で渡された場所のスコアを渡す
    public int GetScore(int index)
    {
        return scoreList_[index];
    }

    //引数で渡された場所の悪魔を倒した数を渡す
    public int GetDevilCount(int index)
    {
        return devilKillList_[index];
    }

    //引数で渡された場所の邪神を倒した数を渡す
    public int GetEvilGodCount(int index)
    {
        return evilGodKillList_[index];
    }

    //今回の結果かどうかを渡す
    public bool GetIsCurrentResult(int index)
    {
        return checkList_[index];
    }
}
