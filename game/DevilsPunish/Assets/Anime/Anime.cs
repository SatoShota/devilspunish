﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Anime : MonoBehaviour
{
    private Animator anim = null;

    public bool animState_ = false;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();

        anim.SetBool("isDead", animState_);
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("isDead", animState_);
    }

    public void SetAnimeState(bool state)
    {
        animState_ = state;
    }
}
